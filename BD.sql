drop table if exists dev_dev_net.Contenu;
drop table if exists dev_dev_net.Avancement;
drop table if exists dev_dev_net.Mouvement;
drop table if exists dev_dev_net.Compte;
drop table if exists dev_dev_net.Beneficiaire;

create table dev_dev_net.Beneficiaire(
	`ID` char(8) PRIMARY KEY,
	`type` enum('club','comite'),
	`nom` varchar(30),
	`mandataire` varchar(30),
    `mdp` varchar(33)
	) ENGINE = InnoDB;


create table dev_dev_net.Compte( 
	`nom` enum('general','aide_developpement','autre'),
	`proprietaireID` char(8),
	`soldeInitial` int default 0,
	PRIMARY KEY ( `nom` , `proprietaireID` ),
	FOREIGN KEY (`proprietaireID`) REFERENCES `Beneficiaire`(`ID`)
	ON UPDATE CASCADE ON DELETE CASCADE 
)ENGINE = 'InnoDB' ;


create table dev_dev_net.Adresse(
	`ID` int(11) PRIMARY KEY,
	`numero` int(11),
	`voie` varchar(40),
	`suite` varchar(100),
	`codePostal` varchar(5),
	`commune` varchar(50)
)ENGINE = InnoDB;


create table dev_dev_net.Reside(
	`beneficiaireID` char(8),
	`adresseID` int(11),
	PRIMARY KEY (`beneficiaireID`,`adresseID`),
	FOREIGN KEY (`beneficiaireID`) 
		references `Beneficiaire` (`ID`)
		ON UPDATE CASCADE ON DELETE CASCADE
	FOREIGN KEY (`adresseID`) 
		references `Adresse` (`ID`)
		ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE = InnoDB;




create table dev_dev_net.Mouvement(
	`ID` int(11),/*la valeur 0 est réservée au panier*/
	`compteID` enum('general','aide_developpement','autre'),
	`beneficiaireID` char(8),
	`adresseID` int(11),
	PRIMARY KEY(`ID`,`compteID`,`beneficiaireID`),
	FOREIGN KEY (`compteID`,`beneficiaireID`)
		references `Compte`( `nom` , `proprietaireID` )
		ON UPDATE CASCADE ON DELETE CASCADE
	FOREIGN KEY (`adresseID` )
		references `Adresse` (`ID`)
		ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE='InnoDB';


create table dev_dev_net.Avancement(
	`mouvementID` int(11),
	`compteID` enum('general','aide_developpement','autre'),
	`beneficiaireID` char(8),
	`etat` enum('panier','cree','valide','refuse'),
	`dateDeValeur` date,/*format 'YYYY-mm-dd' */
	PRIMARY KEY(`mouvementID`,`compteID`,`beneficiaireID`,`etat`),
	FOREIGN KEY (`mouvementID`,`compteID`,`beneficiaireID`) references Mouvement (`ID`,`compteID`,`beneficiaireID`)
	ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE = 'InnoDB';


create table dev_dev_net.Produit(
	`ID` VARCHAR(20),
	`nom` varchar(50),
	`contenu` varchar (100),
	`prix` int(11),
	`destinataire` enum('club','comite'),
	`Catalogue` enum('general','aide_developpement','autre'),
	PRIMARY KEY(`ID`)
)ENGINE = 'InnoDB';


create table dev_dev_net.Contenu(
	`mouvementID` int(11) ,
	`compteID` enum('general','aide_developpement','autre'),
	`beneficiaireID` char(8),
	`produit` VARCHAR(20),
	`quantite` INT(3),
	PRIMARY KEY(`mouvementID`,`CompteID` , `beneficiaireID`,`produit`),
	FOREIGN KEY (`mouvementID`,`compteID`,`beneficiaireID`)
		references `Mouvement`(`ID`,`compteID`,`beneficiaireID` )
		ON UPDATE CASCADE ON DELETE CASCADE
	FOREIGN KEY (`produit`) 
		references `Produit` (`ID`)
		ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE = 'InnoDB';






insert into Beneficiaire values
	('CJE00000','club','du pied','jason toe','alpha'),
	('CJE00001','club','de la main','handy ncap','alpha'),
	('CJE00002','club','du bras','armory brown','alpha'),
	('CJE00003','club','de la jambe','jordan thigh','alpha'),
	('CJE00004','club','du cerveau','jessy pew','alpha'),
	('CJE00005','club','du buste','aron chest','alpha'),
	('CDJE0000','comite','du haut','andy brew','alpha'),
	('CDJE0001','comite','du bas','alpha bravo','alpha');
insert into Adresse values 
	(0,'3','rue de la paix','','75000','Paris'), 
	(1,'4','rue de la paix','','75000','Paris'), 
	(2,'5','rue de la paix','','75000','Paris'), 
	(3,'6','rue de la paix','','75000','Paris'),
	(4,'7','rue de la paix','','75000','Paris'), 
	(5,'8','rue de la paix','','75000','Paris'), 
	(6,'8b','rue de la paix','','75000','Paris'), 
	(7,'8t','rue de la paix','','75000','Paris');
insert into Reside values 
	('CJE00000','0'), 
	('CJE00001','1'), 
	('CJE00002','2'), 
	('CJE00003','3'), 
	('CJE00004','4'), 
	('CJE00005','5'), 
	('CDJE0000','6'), 
	('CDJE0001','7');
/*
ALTER TABLE Produit ADD mailFournisseur varchar(50);


*/
/*insert into values();
ALTER TABLE tbl_name [alter_specification[,alter_specification]...]

alter_specification:
    table_options
  | ADD [COLUMN] col_name column_definition
        [FIRST | AFTER col_name]
  | ADD [COLUMN] (col_name column_definition, ...)
  | ADD [CONSTRAINT [symbol]] PRIMARY KEY
        [index_type] (key_part,...)
        [index_option] ...
  | ADD [CONSTRAINT [symbol]] UNIQUE [INDEX|KEY]
        [index_name] [index_type] (key_part,...)
        [index_option] ...
  | ADD [CONSTRAINT [symbol]] FOREIGN KEY
        [index_name] (col_name,...)
        reference_definition
  | ALTER [COLUMN] col_name {SET DEFAULT literal | DROP DEFAULT}
  | CHANGE [COLUMN] old_col_name new_col_name column_definition
        [FIRST|AFTER col_name]
  | DROP [COLUMN] col_name
  | DROP {INDEX|KEY} index_name
  | DROP PRIMARY KEY
  | DROP FOREIGN KEY fk_symbol
  | MODIFY [COLUMN] col_name column_definition
        [FIRST | AFTER col_name]
  | ORDER BY col_name [, col_name] ...
  | RENAME [TO|AS] new_tbl_name

*/
