-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 27 juin 2019 à 13:50
-- Version du serveur :  5.6.44
-- Version de PHP :  5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `aidedev`
--

-- --------------------------------------------------------

--
-- Structure de la table `avancement`
--

DROP TABLE IF EXISTS `avancement`;
CREATE TABLE IF NOT EXISTS `avancement` (
  `mouvementID` int(11) NOT NULL DEFAULT '0',
  `etat` enum('cree','valide','refuse') NOT NULL DEFAULT 'cree',
  `dateDeValeur` date DEFAULT NULL,
  PRIMARY KEY (`mouvementID`,`etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `avancement`
--

INSERT INTO `avancement` VALUES
(0, 'cree', '2019-06-01'),
(0, 'valide', '2019-06-07'),
(1, 'cree', '2019-06-02'),
(1, 'refuse', '2019-06-08'),
(2, 'cree', '2019-06-03'),
(3, 'cree', '2019-06-04'),
(4, 'cree', '2019-06-05'),
(4, 'refuse', '2019-06-09'),
(5, 'cree', '2019-06-06'),
(5, 'valide', '2019-06-10');

-- --------------------------------------------------------

--
-- Structure de la table `beneficiaire`
--

DROP TABLE IF EXISTS `beneficiaire`;
CREATE TABLE IF NOT EXISTS `beneficiaire` (
  `ID` char(8) NOT NULL,
  `type` enum('club','comite') DEFAULT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `mandataire` varchar(20) DEFAULT NULL,
  `mdp` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `beneficiaire`
--

INSERT INTO `beneficiaire` VALUES
('CDJE0000', 'comite', 'du haut', 'andy brew', 'alpha'),
('CDJE0001', 'comite', 'du bas', 'alpha bravo', ''),
('CJE00000', 'club', 'du pied', 'jason toe', ''),
('CJE00001', 'club', 'de la main', 'handy ncap', ''),
('CJE00002', 'club', 'du bras', 'armory brown', ''),
('CJE00003', 'club', 'de la jambe', 'jordan thigh', ''),
('CJE00004', 'club', 'du cerveau', 'jessy pew', ''),
('CJE00005', 'club', 'du buste', 'aron chest', '');

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `nom` enum('general','aide_developpement','autre') NOT NULL DEFAULT 'general',
  `proprietaireID` char(8) NOT NULL DEFAULT '',
  `soldeInitial` int(11) DEFAULT '0',
  PRIMARY KEY (`nom`,`proprietaireID`),
  KEY `proprietaireID` (`proprietaireID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` VALUES
('general', 'CDJE0000', 0),
('general', 'CDJE0001', 0),
('general', 'CJE00000', 0),
('general', 'CJE00001', 0),
('general', 'CJE00002', 0),
('general', 'CJE00003', 0),
('general', 'CJE00004', 0),
('general', 'CJE00005', 0),
('aide_developpement', 'CDJE0000', 0),
('aide_developpement', 'CJE00000', 100),
('aide_developpement', 'CJE00003', 60),
('aide_developpement', 'CJE00004', 50),
('aide_developpement', 'CJE00005', 800);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcdje0000`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcdje0000`;
CREATE TABLE IF NOT EXISTS `datasforcdje0000` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcdje0001`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcdje0001`;
CREATE TABLE IF NOT EXISTS `datasforcdje0001` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcdje0005`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcdje0005`;
CREATE TABLE IF NOT EXISTS `datasforcdje0005` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00000`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00000`;
CREATE TABLE IF NOT EXISTS `datasforcje00000` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00001`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00001`;
CREATE TABLE IF NOT EXISTS `datasforcje00001` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00002`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00002`;
CREATE TABLE IF NOT EXISTS `datasforcje00002` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00003`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00003`;
CREATE TABLE IF NOT EXISTS `datasforcje00003` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00004`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00004`;
CREATE TABLE IF NOT EXISTS `datasforcje00004` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `datasforcje00005`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `datasforcje00005`;
CREATE TABLE IF NOT EXISTS `datasforcje00005` (
`ID` char(8)
,`type` enum('club','comite')
,`nom` varchar(20)
,`mandataire` varchar(20)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcdje0000`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcdje0000`;
CREATE TABLE IF NOT EXISTS `fulldatasforcdje0000` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcdje0001`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcdje0001`;
CREATE TABLE IF NOT EXISTS `fulldatasforcdje0001` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcdje0005`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcdje0005`;
CREATE TABLE IF NOT EXISTS `fulldatasforcdje0005` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00000`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00000`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00000` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00001`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00001`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00001` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00002`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00002`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00002` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00003`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00003`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00003` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00004`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00004`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00004` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `fulldatasforcje00005`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `fulldatasforcje00005`;
CREATE TABLE IF NOT EXISTS `fulldatasforcje00005` (
`nom` enum('general','aide_developpement','autre')
,`proprietaireID` char(8)
,`soldeInitial` int(11)
,`ID` int(11)
,`descriptif` varchar(200)
,`montant` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`beneficiaireID` char(8)
,`mouvementID` int(11)
,`etat` enum('cree','valide','refuse')
,`dateDeValeur` date
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `logins`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
`ID` char(8)
,`mdp` varchar(32)
);

-- --------------------------------------------------------

--
-- Structure de la table `mouvement`
--

DROP TABLE IF EXISTS `mouvement`;
CREATE TABLE IF NOT EXISTS `mouvement` (
  `ID` int(11) NOT NULL,
  `descriptif` varchar(200) DEFAULT NULL,
  `montant` int(11) DEFAULT NULL,
  `compteID` enum('general','aide_developpement','autre') DEFAULT NULL,
  `beneficiaireID` char(8) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `compteID` (`compteID`,`beneficiaireID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mouvement`
--

INSERT INTO `mouvement` VALUES
(0, 'licences', -500, 'general', 'CJE00000'),
(1, 'pack tournoi', -60, 'aide_developpement', 'CJE00000'),
(2, 'recharge', 120, 'aide_developpement', 'CJE00000'),
(3, 'licences', 20, 'general', 'CJE00001'),
(4, 'recharge', 60, 'aide_developpement', 'CDJE0000'),
(5, 'pack CDJE', -40, 'aide_developpement', 'CDJE0000');

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcdje0000`
--
DROP TABLE IF EXISTS `datasforcdje0000`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcdje0000`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CDJE0000') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcdje0001`
--
DROP TABLE IF EXISTS `datasforcdje0001`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcdje0001`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CDJE0001') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcdje0005`
--
DROP TABLE IF EXISTS `datasforcdje0005`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcdje0005`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CDJE0005') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00000`
--
DROP TABLE IF EXISTS `datasforcje00000`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00000`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00000') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00001`
--
DROP TABLE IF EXISTS `datasforcje00001`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00001`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00001') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00002`
--
DROP TABLE IF EXISTS `datasforcje00002`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00002`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00002') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00003`
--
DROP TABLE IF EXISTS `datasforcje00003`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00003`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00003') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00004`
--
DROP TABLE IF EXISTS `datasforcje00004`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00004`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00004') ;

-- --------------------------------------------------------

--
-- Structure de la vue `datasforcje00005`
--
DROP TABLE IF EXISTS `datasforcje00005`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasforcje00005`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`type` AS `type`,`beneficiaire`.`nom` AS `nom`,`beneficiaire`.`mandataire` AS `mandataire`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` where (`beneficiaire`.`ID` = 'CJE00005') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcdje0000`
--
DROP TABLE IF EXISTS `fulldatasforcdje0000`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcdje0000`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CDJE0000') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcdje0001`
--
DROP TABLE IF EXISTS `fulldatasforcdje0001`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcdje0001`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CDJE0001') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcdje0005`
--
DROP TABLE IF EXISTS `fulldatasforcdje0005`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcdje0005`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CDJE0005') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00000`
--
DROP TABLE IF EXISTS `fulldatasforcje00000`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00000`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00000') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00001`
--
DROP TABLE IF EXISTS `fulldatasforcje00001`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00001`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00001') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00002`
--
DROP TABLE IF EXISTS `fulldatasforcje00002`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00002`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00002') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00003`
--
DROP TABLE IF EXISTS `fulldatasforcje00003`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00003`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00003') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00004`
--
DROP TABLE IF EXISTS `fulldatasforcje00004`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00004`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00004') ;

-- --------------------------------------------------------

--
-- Structure de la vue `fulldatasforcje00005`
--
DROP TABLE IF EXISTS `fulldatasforcje00005`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fulldatasforcje00005`  AS  select `compte`.`nom` AS `nom`,`compte`.`proprietaireID` AS `proprietaireID`,`compte`.`soldeInitial` AS `soldeInitial`,`mouvement`.`ID` AS `ID`,`mouvement`.`descriptif` AS `descriptif`,`mouvement`.`montant` AS `montant`,`mouvement`.`compteID` AS `compteID`,`mouvement`.`beneficiaireID` AS `beneficiaireID`,`avancement`.`mouvementID` AS `mouvementID`,`avancement`.`etat` AS `etat`,`avancement`.`dateDeValeur` AS `dateDeValeur` from ((`compte` left join `mouvement` on(((`mouvement`.`compteID` = `compte`.`nom`) and (`compte`.`proprietaireID` = `mouvement`.`beneficiaireID`)))) left join `avancement` on((`avancement`.`mouvementID` = `mouvement`.`ID`))) where (`compte`.`proprietaireID` = 'CJE00005') ;

-- --------------------------------------------------------

--
-- Structure de la vue `logins`
--
DROP TABLE IF EXISTS `logins`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `logins`  AS  select `beneficiaire`.`ID` AS `ID`,`beneficiaire`.`mdp` AS `mdp` from `beneficiaire` ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `avancement`
--
ALTER TABLE `avancement`
  ADD CONSTRAINT `avancement_ibfk_1` FOREIGN KEY (`mouvementID`) REFERENCES `mouvement` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `compte_ibfk_1` FOREIGN KEY (`proprietaireID`) REFERENCES `beneficiaire` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mouvement`
--
ALTER TABLE `mouvement`
  ADD CONSTRAINT `mouvement_ibfk_1` FOREIGN KEY (`compteID`,`beneficiaireID`) REFERENCES `compte` (`nom`, `proprietaireID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
