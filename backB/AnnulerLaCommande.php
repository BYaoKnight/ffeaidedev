<?php // requrire needed scripts and class declaration
	require_once "../backB/CheckConnection.php";
	require_once "../back/connexionBD.php";
	require_once "../back/classes1.php";
	require_once "../loadB/loadBeneficiaire.php";
	require_once "../loadB/waitingMoves.php";


if(isset($_POST['mouvement'])){
	$notok=true;
	$unchain=preg_split("#[|]#", $_POST['mouvement']);
	$compte=$unchain[0];
	$mvtID=$unchain[1];

	$result=$bdd->beginTransaction();
	//===================================  Transaction  ===========================================
		$result=$bdd->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
		$sql="SELECT COUNT(*) as count FROM WaitingMoves WHERE mvtID='$mvtID' AND compte='$compte' AND beneficiaire='".$_SESSION['beneficiaireID']."'";
		//echo "<pre>$sql</pre>";
		$result=$bdd->query($sql);
		if(!$result){
			$_SESSION['msg'].= "error 1 annulCmd";
			goto endOfTransaction;
		}
		if ($result->fetch()['count']) {
			$sql="DELETE FROM Mouvement WHERE ID='$mvtID' AND compteID='$compte' AND beneficiaireID='".$_SESSION['beneficiaireID']."'";
			$result=$bdd->query($sql);
			if(!$result){
				$_SESSION['msg'].= "error 2 annulCmd";
				goto endOfTransaction;
			}
		}
		else{
			$_SESSION['msg'].= "la commande a déjà été validée, refusée ou annulée";
		}
	$result=$bdd->commit();
	$notok=false;
	$_SESSION['msg']= "votre commande a bien été supprimée";

	endOfTransaction:
	if($notok){$bdd->rollback();}
	
}
//echo "<pre>".$_SESSION['msg']."</pre>";
header("Location: ../beneficiaire/mvtEnAttente.php");	exit();