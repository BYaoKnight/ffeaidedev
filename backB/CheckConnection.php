<?php
if (!isset($_SESSION)) {
	session_start();
}
if(!isset($_SESSION['beneficiaireID'])){
	$_SESSION['errorTXT']='Suite à une période d\inactivité trop longue, vous avez été déconnecté(e)';
	header('Location: ../beneficiaire/login.php');
	exit();
}
?>