<?php
require_once "../backG/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require_once "../classes/Constantes.php";

$_SESSION['msg']='';
if ($_POST AND isset($_POST['submit']) AND isset($_POST['mvt']) AND preg_match("#(valid|refus)er#",$_POST['submit']) ) {
$etat=substr($_POST['submit'],0,-1);
$pdtFournisseur=array();
$beneficiaires=array();
$adresses=array();
/*
$pdtFournisseur['Fournisseur'] (['beneficiaireID']['adresseNom']) ['produitQtt']
*/
$ouverture="<html><body><pre>\tbonjour ";
$signature="\r\n\r\nCordialement\r\n\r\nle gestionnaire de FFE";
$fermeture="</pre></body></html>";

foreach ($_POST['mvt'] as $key => $value) {

	$array=preg_split('#/#',$value,-1,PREG_SPLIT_NO_EMPTY);
	$beneficiaireID=$array[0];
	$compte=$array[1];
	$mvt=$array[2];
		
	//========================  transaction SQL  ==================================================
		$result=$bdd->beginTransaction();
		$transactionOK=true;
		//=======================  Avancer le mouvement  ==========================================
			$result=$bdd->query("INSERT INTO Avancement VALUES ('$mvt','$compte','$beneficiaireID','$etat','".date('Y/m/d')."');");
			if(!$result){
				var_dump($result);
				$_SESSION['msg'].="\nerreur BD avancement";
				$transactionOK=false;
				goto endOfTransaction;
			}
		//=========================  beneficiaire  ================================================
			if (!isset($beneficiaires[$beneficiaireID])) {
			$result=$bdd->query("SELECT * FROM Beneficiaire WHERE ID='$beneficiaireID';");
			if(!$result){
				var_dump($result);
				$_SESSION['msg'].="\nerreur BD beneficiaire";
				$transactionOK=false;
				goto endOfTransaction;
			}
			$benef=$result->fetch();
			$beneficiaires[$beneficiaireID]=new Club($beneficiaireID,$benef['Commune'],$benef['nom'],$benef['mandataire'],$benef['email']);
			}
		//============================  adresse  ==================================================
			$result=$bdd->query("SELECT * From Adresse JOIN Mouvement ON adresseNom=Adresse.surnom AND Mouvement.beneficiaireID=Adresse.beneficiaireID WHERE ID='$mvt' AND compteID='$compte' AND Adresse.beneficiaireID='$beneficiaireID' ;");
			if(!$result){
				var_dump($result);
				$_SESSION['msg'].="\nerreur BD Adresse";
				$transactionOK=false;
				goto endOfTransaction;
			}
			$rslt=$result->fetch();
			$adresseID=$rslt['adresseNom'];
			if(!isset($adresses[($beneficiaireID.'|'.$adresseID)])){
				$adresses[($beneficiaireID.'|'.$adresseID)]=new 
					Adresse($rslt['numero'],$rslt['voie'],$rslt['suite'],$rslt['codePostal'],$rslt['commune']);
			}
		//======================  telecharger les produits  =======================================
			$sql="SELECT produit,quantite,nom,contenu,prix,fournisseurID FROM Contenu JOIN Produit ON produit=ID WHERE mouvementID='$mvt' AND compteID='$compte' AND beneficiaireID='$beneficiaireID' ;";
			$result=$bdd->query($sql);
			if(!$result){
				var_dump($result);
				$_SESSION['msg'].="\nerreur BD Contenu";
				$transactionOK=false;
				goto endOfTransaction;
			}
			while($rslt=$result->fetch()){
				$fournisseurID=($_POST['submit']=='valider'?$rslt['fournisseurID']:NULL);
				if(!isset($pdtFournisseur[$fournisseurID])){
					$pdtFournisseur[$fournisseurID]=array();
				}
				if(!isset($pdtFournisseur[$fournisseurID][$beneficiaireID])){
					$pdtFournisseur[$fournisseurID][$beneficiaireID]=array();
				}
				if(!isset($pdtFournisseur[$fournisseurID][$beneficiaireID][$adresseID])){
					$pdtFournisseur[$fournisseurID][$beneficiaireID][$adresseID]=array();
				}
				if (!isset($pdtFournisseur[$fournisseurID][$beneficiaireID][$adresseID][$rslt['produit']])) {
					$pdtFournisseur[$fournisseurID][$beneficiaireID][$adresseID][$rslt['produit']] = new ProduitQuantite(new Produit($rslt['produit'],$rslt['nom'],$rslt['contenu'],$rslt['prix']),$rslt['quantite']);
				}
				else{$pdtFournisseur[$fournisseurID][$beneficiaireID][$adresseID][$rslt['produit']]->quantite+=$rslt['quantite'];}
				
			}
			$bdd->commit();
		endOfTransaction:
		if(!$transactionOK){$bdd->rollback();}
}
echo "<pre>";
var_dump($pdtFournisseur);
var_dump($beneficiaires);
var_dump($adresses);

foreach ($pdtFournisseur as $fournisseurID => $pdtBenef) {
	//==============================  fournisseur  ================================================
		$debuggedFournisseurID=($_SESSION['gestionnaireID']=="debugger")?"11111":"").$fournisseurID;
		if ($fournisseurID!=NULL) {
			$sql="SELECT * FROM Fournisseur WHERE ID='$debuggedFournisseurID'";
			$result=$bdd->query($sql);
			while ($rslt=$result->fetch()) {
				$fournisseur=new Fournisseur($rslt['ID'],$rslt['nom'],$rslt['email']);
			}
			$intro=$fournisseur->nom.",\r\n";
		}
		else{$fournisseur=new Fournisseur(-1,'zero',MAIL_GESTIONNAIRE);}

	foreach ($pdtBenef as $beneficiaireID => $pdtAdresse) {
		$beneficiaire=$beneficiaires[$beneficiaireID];
		$typeCommunication=0;
		$prixTotal=0;
		$corpsMail='';
		$introBenef="le ".$beneficiaire->type().' '.$beneficiaire->nom." commande ";
		$introAdresse='';
		$txtsousTotal='';
		foreach ($pdtAdresse as $adresseNom => $pdtQtt) {
			echo ($beneficiaireID.'|'.$adresseNom)."\n";
			$adresse=$adresses[($beneficiaireID.'|'.$adresseNom)];
			$sousTotal=0;

			$introAdresse.=$txtsousTotal."à l'adresse suivante\r\n$adresse\r\nles packs suivants :\r\n";
			$introAddress.=$txtsousTotal."à l'adresse suivante\r\n$adresse\r\n et contenait les packs suivants :\r\n";
			foreach ($pdtQtt as $key => $value) {
				if($value->produit->name=='Communication' AND $typeCommunication<2){$typeCommunication=2;}
				if($value->produit->name=='mixte' AND $typeCommunication==0){$typeCommunication=1;}
				$sousTotal+=$value->prixPartiel();
				$corpsMail.='- Pack '.$value->produit->name;
				/*$corpsMail.="\r\n\t\t".$value->produit->contenu;*/
				$corpsMail.="\r\n\tPrix Unitaire : ".$value->produit->prix.'€';
				$corpsMail.="\r\n\t Quantité   :   ".$value->quantite;
				$corpsMail.="\r\n\tPrix Partiel :  ".$value->prixPartiel()."€\r\n";
			}
			$txtsousTotal="pour un sous-total de $sousTotal €\r\n\r\n";
			$introAdresse.=$corpsMail;
			$introAddress.=$corpsMail;
			$prixTotal+=$sousTotal;
		}
		if($sousTotal==$prixTotal){$txtsousTotal="\r\n";}
		$corpsEmail.=$introBenef.$introAdresse.$txtsousTotal."pour un total de $prixTotal €";
		$corpsemail.=$introAddress.$txtsousTotal."pour un total de $prixTotal €";
		//=======================  envoi du mail  =================================================
			if ($_POST['submit']=='valider') {
				if($typeCommunication){
					$messagePS="\r\n\r\nP.S. à destination du club:\r\nPour que l'imprimeur puisse personnaliser vos produits, veuillez leur envoyer les éléments nécessaires tels que :le nom de votre club, le logo de votre club (de la meilleur qualité possible, ou vectorisé si vous avez), vos coordonnées (celles du siège et/ou de la salle de jeux)";
					if ($typeCommunication==2) {
						$messagePS.=", ainsi que les informations que vous désirez inscrire sur la carte de visite";
					}
					$messagePS.='.';
				}
				$header=MAIL_HEADERS."cc: ".MAIL_GESTIONNAIRE." ,".$beneficiaire->email."\r\n";
				$mail=$ouverture.$intro.$corpsEmail.$signature.$messagePS.$fermeture;
				if(!mail($fournisseur->email,'FFE - commande aide developpement',$mail,$header)){
					$log_commandes=fopen('../logs/commandes.txt','a');
					fputs($log_commandes,date('[D Y-m-d @ H:i e]')." la commande $mvt de $beneficiaire sur le compte '$compte' n'a pas été transmise au fournisseur;\r\n".$message."\r\n\r\n");
					fclose($log_commandes);
					$_SESSION['msg']="le mail n'a pas pu être envoyé à ".$fournisseur->nom;
				}
				else{
					$_SESSION['msg'].="tout s'est bien passé pour ".$fournisseur->nom;
				}
			}
			else{
				$header=MAIL_HEADERS."cc: ".MAIL_GESTIONNAIRE."\r\n";
				$intro=$beneficiaire->mandataire.",\r\n";
				$intro.="Une ou plusieurs des commandes pour le ".$beneficiaire->type().' '.$beneficiaire->nom." ont été refusées.\r\nElles devaient être livrées ";
				$mail=$ouverture.$intro.$corpsemail.$signature.$messagePS.$fermeture;
				if(!mail($beneficiaire->email,'FFE - commande aide developpement',$mail,$header)){
					$log_commandes=fopen('../logs/commandes.txt','a');
					fputs($log_commandes,date('[D Y-m-d @ H:i e]')." le $beneficiaire n'a pas été informé que la commande $mvt sur le compte '$compte' a été refusé;\r\n".$mail."\r\n\r\n");
					fclose($log_commandes);
					$_SESSION['msg']="le mail n'a pas pu être envoyé à ".$fournisseur->nom;
				}
				else{
					$_SESSION['msg'].="tout s'est bien passé pour ".$fournisseur->nom;
				}
			}
		//=============================  effacer les variables  ==================================
			$corpsMail='';
			$corpsemail='';
			$corpsEmail='';
			$txtsousTotal='';
			$messagePS='';
			$introAddress='';
			$introBenef='';
			$introAdresse='';
	}
}

}




header('location : /gestionnaire/TableauDeBord.php');
exit();




?>
