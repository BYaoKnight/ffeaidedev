<?php
require_once "../backG/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require_once "../classes/Constantes.php";

$_SESSION['msg']='';
if ($_POST AND isset($_POST['submit']) AND isset($_POST['data']) AND preg_match("#(valid|refus)er#",$_POST['submit']) ) {
	//========================  initialisation de variables  ======================================
		$array=preg_split('#/#',$_POST['data'],-1,PREG_SPLIT_NO_EMPTY);
		$beneficiaireID=$array[0];
		$compte=$array[1];
		$mvt=$array[2];
		$etat=substr($_POST['submit'],0,-1);

		$pdtFournisseur=array();
		/*
		$pdtFournisseur['Fournisseur'] (['Beneficiaire']['Adresse']) ['produitQtt']
		*/

	//========================  transaction SQL  ==================================================
		$result=$bdd->beginTransaction();
		$transactionOK=true;
		//========================= Avancer le mouvement ==============================================
			$result=$bdd->query("INSERT INTO Avancement VALUES ('$mvt','$compte','$beneficiaireID','$etat','".date('Y/m/d')."');");
			if(!$result){//		var_dump($result);
				$_SESSION['msg']="erreur BD1";
				$transactionOK=false;
				goto endOfTransaction;
			}
		//========================== telecharger les produits =========================================
			$sql="SELECT produit,quantite,nom,contenu,prix,fournisseurID FROM Contenu JOIN Produit ON produit=ID WHERE mouvementID='$mvt' AND compteID='$compte' AND beneficiaireID='$beneficiaireID' ;";
			$result=$bdd->query($sql);
			if(!$result){//		var_dump($result);
				$_SESSION['msg']="erreur BD2";
				$transactionOK=false;
				goto endOfTransaction;
			}
			while($rslt=$result->fetch()){
				if(!isset($pdtFournisseur[$rslt['fournisseurID']])){
					$pdtFournisseur[$rslt['fournisseurID']]=array();
				}
				$pdtFournisseur[$rslt['fournisseurID']][$rslt['produit']] = new ProduitQuantite(new Produit($rslt['produit'],$rslt['nom'],$rslt['contenu'],$rslt['prix']),$rslt['quantite']);
			}
		//==========================  adresse  ========================================================
			$result=$bdd->query("SELECT * From Adresse JOIN Mouvement ON adresseNom=Adresse.surnom AND Mouvement.beneficiaireID=Adresse.beneficiaireID WHERE Mouvement.ID='$mvt' AND compteID='$compte' AND Mouvement.beneficiaireID='$beneficiaireID' ;");
			if(!$result){//		var_dump($result);
				$_SESSION['msg']="erreur BD3";
				$transactionOK=false;
				goto endOfTransaction;
			}
			$rslt=$result->fetch();
			$adresse=new Adresse($rslt['numero'],$rslt['voie'],$rslt['suite'],$rslt['codePostal'],$rslt['commune']);
		//==============================  nom beneficiaire  ===========================================
			$result=$bdd->query("SELECT * FROM Beneficiaire WHERE ID='$beneficiaireID';");
			if(!$result){//		var_dump($result);
				$_SESSION['msg']="erreur BD4";
				$transactionOK=false;
				goto endOfTransaction;
			}
			$benef=$result->fetch();
			$beneficiaire=new Club($beneficiaireID,$benef['Commune'],$benef['nom'],$benef['mandataire'],$benef['email']);
		$bdd->commit();
		endOfTransaction:
		if (!$transactionOK){$bdd->rollback();}

	//=========================  envoi du Mail  ===================================================
		//echo "\n\npdtFournisseur\n";var_dump($pdtFournisseur);echo "\n\nbeneficiaire\n";var_dump($beneficiaire);echo "\n\nadresse\n";var_dump($adresse);
	
		foreach ($pdtFournisseur as $fournisseurID => $pdtsqtt) {
			//var_dump($fournisseurID);var_dump($pdtsqtt);
			$typeCommunication=0;
			
			//======================  fournisseur  ================================================
				$debuggedFournisseurID=($_SESSION['gestionnaireID']=="debugger")?"11111":"").$fournisseurID;
				$sql="SELECT * FROM Fournisseur WHERE ID='$fournisseurID'";
				$result=$bdd->query($sql);
				while ($rslt=$result->fetch()) {
					$fournisseur=new Fournisseur($rslt['ID'],$rslt['nom'],$rslt['email']);
				}

			
			//echo "\n\n$header\n\n";
			//=====================  corps du mail ================================================
				$ouverture="<html><body><pre>\tbonjour ";
				$signature="Cordialement\r\n\r\nle gestionnaire de FFE";
				$fermeture="</pre></body></html>";
				
				$prixTotal=0;
				$corpsMail='';
				foreach ($pdtsqtt as $key => $value) {
					if($value->produit->name=='Communication' AND $typeCommunication<2){$typeCommunication=2;}
					if($value->produit->name=='mixte' AND $typeCommunication==0){$typeCommunication=1;}
					$prixTotal+=$value->prixPartiel();
					$corpsMail.='- Pack '.$value->produit->name;
					/*$corpsMail.="\r\n\t".$value->produit->contenu;*/
					$corpsMail.="\r\n\tPrix Unitaire : ".$value->produit->prix.'€';
					$corpsMail.="\r\n\t Quantité   :   ".$value->quantite;
					$corpsMail.="\r\n\tPrix Partiel :  ".$value->prixPartiel()."€\r\n";
				}
				$corpsMail.="\r\nPour un montant total de $prixTotal €\r\n\r\n";
				$messagePS='';

			if ($_POST['submit']=='valider') {
				$intro=$fournisseur->nom.",\r\n";
				$intro.="le ".$beneficiaire->type().' '.$beneficiaire->nom." commande à ";
				$intro.="l'adresse suivante\r\n$adresse\r\n\r\nles packs suivants :\r\n";
				if($typeCommunication){
					$messagePS.="\r\n\r\nP.S. à destination du club:\r\nPour que l'imprimeur puisse personnaliser vos produits, veuillez leur envoyer les éléments nécessaires tels que :le nom de votre club, le logo de votre club (de la meilleur qualité possible, ou vectorisé si vous avez), vos coordonnées (celles du siège et/ou de la salle de jeux)";
					if ($typeCommunication==2) {
						$messagePS.=", ainsi que les informations que vous désirez inscrire sur la carte de visite";
					}
					$messagePS.='.';
				}
				$header=MAIL_HEADERS."cc: ".MAIL_GESTIONNAIRE." ,".$beneficiaire->email."\r\n";
				$mail=$ouverture.$intro.$corpsMail.$signature.$messagePS.$fermeture;
				if(!mail($fournisseur->email,'FFE - commande aide developpement',$mail,$header)){
					$log_commandes=fopen('../logs/commandes.txt','a');
					fputs($log_commandes,date('[D Y-m-d @ H:i e]')." la commande $mvt de $beneficiaire sur le compte '$compte' n'a pas été transmise au fournisseur;\r\n".$message."\r\n\r\n");
					fclose($log_commandes);
					$_SESSION['msg']="le mail n'a pas pu être envoyé à ".$fournisseur->nom;
				}
				else{
					$_SESSION['msg'].="tout s'est bien passé pour ".$fournisseur->nom;
				}
			}
			else{
				$header=MAIL_HEADERS."cc: ".MAIL_GESTIONNAIRE."\r\n";
				$intro=$beneficiaire->mandataire.",\r\n";
				$intro.="Une des commandes pour le ".$beneficiaire->type().' '.$beneficiaire->nom." a été refusée.\r\nElle devait être livrée à ";
				$intro.="l'adresse suivante\r\n$adresse\r\n\r\n et contenait les packs suivants :\r\n";
				$mail=$ouverture.$intro.$corpsMail.$signature.$messagePS.$fermeture;
				if(!mail($beneficiaire->email,'FFE - commande aide developpement',$mail,$header)){
					$log_commandes=fopen('../logs/commandes.txt','a');
					fputs($log_commandes,date('[D Y-m-d @ H:i e]')." le $beneficiaire n'a pas été informé que la commande $mvt sur le compte '$compte' a été refusé;\r\n".$mail."\r\n\r\n");
					fclose($log_commandes);
					$_SESSION['msg']="le mail n'a pas pu être envoyé à ".$fournisseur->nom;
				}
				else{
					$_SESSION['msg'].="tout s'est bien passé pour ".$fournisseur->nom;
				}
			}
		}
	
}


header('location : /gestionnaire/TableauDeBord.php');
exit();
?>
