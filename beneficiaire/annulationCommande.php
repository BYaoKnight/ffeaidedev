<?php // requrire needed scripts and class declaration
	require_once "../backB/CheckConnection.php";
	require_once "../back/connexionBD.php";
	require_once "../back/classes1.php";
	require_once "../loadB/loadBeneficiaire.php";
	require_once "../loadB/waitingMoves.php";
$pageTitle='Supprimer une commande';
//echo "<pre>";var_dump($_POST);echo "</pre>";
if (!isset($_POST['mouvement'])) {
	header("Location: mvtEnAttente.php");	exit();
	//return;

}
$unchain=preg_split("#[|]#", $_POST['mouvement']);
$compteID=$unchain[0];
$mvtID=$unchain[1];
foreach ($_SESSION['beneficiaire']->comptes as $compte) {
	if($compte->type==$compteID){
		foreach ($compte->mouvements() as $mvt) {
			if ($mvt->ID()==$mvtID) {
				$date=$mvt->avancement()->date;
				$contenu=$mvt->description();
			}
		}
	}
}
if (!isset($date) OR !isset($contenu)) {
	header("Location: mvtEnAttente.php");	exit();
	//echo "not found";	return;
}
?>
<!DOCTYPE HTML>
<html>
	<?php require_once "head.php";?>
<body>
	<?php require_once "header.php";?>



<section>
	<p>Vous êtes sur le point de supprimer la commande que vous avez effectué le <em><?=$date?></em> contenant <em><?=$contenu?></em> </p>
	<form action="../backB/AnnulerLaCommande.php" method="POST">
		<a href="mvtEnAttente.php"><button type="button">Retour</button></a>
		<input type="hidden" name="mouvement" value="<?=$_POST['mouvement']?>">
		<input type="submit" name="submit" value="Supprimer la commande">
	</form>
</section>
</body>
</html>
