<?php 
require_once "../backB/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require_once "../loadB/loadBeneficiaire.php";
require_once "../loadB/loadCatalogues.php";
$pageTitle='Catalogue';
?>


<!DOCTYPE HTML>
<html>
<?php require_once "head.php";?>
<body>

<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";
?>

<!-------------------corps du catalogue-------------------------------------->


<!-- critère de recherche -->

<section class="gestionAffichage">
<?php

//var_dump($_SESSION['catalogues']);
$catalogue=end($_SESSION['catalogues']);
$key=key($_SESSION['catalogues']);
echo '<h2>'.(preg_match("#aide_developpement#",$key)?' d\'aide au développement ':' général ').
	(preg_match("#Club#",$key)?' des clubs':' des comités').'</h2>';
	//var_dump($catalogue);

//-------------------transaction ajout au panier-------------------------------------->
require_once "../backB/catalogue.php";
?>
</section> 



<section class="produits">
	<?php 
	foreach ($catalogue->produits as $produit) {?>
		<div class="produit">
			<a href="detailsProduit.php?produit=<?=$produit->ID?>"><img src="<?= $produit->urlImage() ?>"></a>
			<h3 class="h"><?=$produit->name?></h3> <h4 class="h"><?=$produit->prix?>€</h4>
			<div class="catalinks l">
				
			</div>
			<div class="catalinks r">
				<form action="catalogue.php" method="post" >
					<input type="HIDDEN" name="produit" value="<?=$produit->ID?>">
					<input type="HIDDEN" name="produitnom" value="<?=$produit->name?>">
					<input type=submit value="panier+" >
				</form>
			</div>
			<details class="catalinks l">
				<summary>détails</summary>
				<?=$produit->contenu?>
			</details>
		</div>
		<?php
	}
	?>
</section>

