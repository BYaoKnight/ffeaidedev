<?php 
require_once "../backB/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require "../loadB/loadBeneficiaire.php";
require "../loadB/loadPanier.php";
if (! ($_POST AND isset($_POST['compte']) AND
	($compte=$_SESSION['beneficiaire']->getCompte($_POST['compte']))) ) {

	header('Location: http://'.$_SERVER['SERVER_NAME'].'/ffeAideDev/beneficiaire/panier.php');
	exit();
}
else {	
$pageTitle='Confirmation Panier';
?>



<!DOCTYPE HTML>
<html>
<?php require_once "head.php";?>
<body>



<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";


//=================================== affichage de la commande ===============================


$solde=$compte->solde();
$encours=$compte->encours();
$prixTotal=$compte->mouvements()[0]->prixTotal();
$a=($prixTotal<=$solde);
$b=($prixTotal<=($solde+$encours));
$c=($a AND $b);

if(!$c){
	header('Location: /beneficiaire/panier.php');
	exit();
}
else{
echo '<form action="../backB/commande.php" method="post">
	<section class="panier"><h2 style="margin-top:0px;">', $compte->type ,'</h2>';
?>
<table>
	<tr>
		<td class="panier" colspan="5">
			solde : <?= $compte->solde().' €'. $compte->printEC() ?>
		</td>
	</tr>
	<tr>
		<th></th>
		<th width="50%">nom</th>
		<th width="90">prix unitaire</th>
		<th width="60">quantité</th>
		<th width="50">montant</th>
	</tr>
<?php
//var_dump($compte);
foreach ($compte->mouvements()[0]->produitsQtt() as $pdtqtt) {
	if($pdtqtt->quantite>0){
		echo '
		<tr class="beta">
			<td>►</td>
			<td>', $pdtqtt->produit->name ,'</td>
			<td>', $pdtqtt->produit->prix ,'</td>
			<td>', $pdtqtt->quantite ,'</td>
			<td>', $pdtqtt->prixPartiel() ,'</td>
		</tr>';
	}
}
echo '
	<tr>
		<th colspan="4" align="right">TOTAL</th>
		<th '.($c?'':('style="background-color:red;"')).'>'. $prixTotal.'</th>
	</tr>
</table>
</section>
<section class=adresses>';


//======================  Adresses  ===========================================
$first=' checked';
foreach ($_SESSION['beneficiaire']->adresses as $key => $adresse) {?>
	<div class="adresse">
		<input type="radio" name="adresse" value="<?= $key?>" <?= $first ?>>
		<h3 style="display: inline-block;"><?= $key ?></h3>
		<p><?= $adresse?></p>	
	</div>
	<?php
	$first='';
}
if($first==' checked'){
	echo "<h2> Vous devez enregistrer une <a href=\"Adresses.php\">adresse de livraison</a> pour pouvoir commander.</h2>";
	$first=' disabled';
}
?>
</section><br>
<a href="panier.php"><button type="button">retour</button></a>
<input type="hidden" name="compte" value="<?= $compte->type?>">
<input type="submit" value="confirmer" <?=$first?> >
<?php
}
}