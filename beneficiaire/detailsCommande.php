<?php // requrire needed scripts and class declaration
	require_once "../backB/CheckConnection.php";
	require_once "../back/connexionBD.php";
	require_once "../back/classes1.php";
	require_once "../loadB/loadBeneficiaire.php";
	//require_once "../loadB/.php";
$pageTitle='Détails Commande';
if(!$_POST OR !isset($_POST['mouvement'])){
	header('Location: historiquecmd.php');
	exit();
}
$unchain=preg_split("#[|]#", $_POST['mouvement']);
$compteID=$unchain[0];
$mvtID=$unchain[1];
$benefID=$_SESSION['beneficiaireID'];
if (isset($_POST['commentaire'])) {
	$bdd->query("UPDATE Mouvement SET commentaire='".$_POST['commentaire']."' WHERE ID='$mvtID' AND compteID='$compteID' AND beneficiaireID='$benefID';");
}
$mouvement=new Mouvement($mvtID,$compteID,$benefID);

$result=$bdd->query("SELECT * FROM Mvt_Adresse WHERE mvtID='$mvtID' AND compte='$compteID' AND beneficiaire='$benefID';");
//echo "<pre>";var_dump($result);echo "</pre>";
while ($rslt=$result->fetch()) {
	$mouvement->ajoutAdresse(new Adresse($rslt['numero'],$rslt['voie'],$rslt['suite'],$rslt['codePostal'],$rslt['commune']));
	$mouvement->commentaire=$rslt['commentaire'];
	//var_dump($mouvement);
}

$result=$bdd->query("SELECT * FROM Avancement WHERE mouvementID='$mvtID' AND compteID='$compteID' AND beneficiaireID='$benefID' ORDER BY etat;");
while ($rslt=$result->fetch()) {
	$mouvement->avancer(new Avancement($rslt['dateDeValeur'],$rslt['etat']));
}

$result=$bdd->query("SELECT * FROM ProduitQtt WHERE mvtID='$mvtID' AND compteID='$compteID' AND benefID='$benefID';");
while ($rslt=$result->fetch()) {
	$mouvement->ajouterProduit(new Produit($rslt['ID'],$rslt['nom'],$rslt['contenu'],$rslt['prix']),$rslt['quantite']);
}


?>
<!DOCTYPE HTML>
<html>
	<?php require_once "head.php";?>
<body>
	<?php require_once "header.php";?>


<!-------------------corps du tableau de bord-------------------------------------->
<section class="divers">
	<h3>Adresse de livraison</h3>
	<?=$mouvement->adresseLivraison ?>
</section>
<section class="divers">
	<h3>Etat d'avancement</h3>
	<table>
		<thead>
			<tr>
				<th> état </th>
				<th> date </th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($mouvement->avancement as $key => $avnct) {?>
			<tr>
				<td> <?=$avnct->etat ?> </td>
				<td> <?=$avnct->date ?> </td>
			</tr>
			<?php
		}?>
		</tbody>
	</table>
</section>

<section class="divers">
	<h3>Contenu</h3>
	<table>
		<thead>
			<tr>
				<th> Produit </th>
				<th> Prix Unitaire</th>
				<th> Quantité </th>
				<th> montant </th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($mouvement->produitsQtt() as $key => $pdtqtt) {
			?>
			<tr>
				<td><?=$pdtqtt->produit->name?></td>
				<td><?=$pdtqtt->produit->prix?></td>
				<td><?=$pdtqtt->quantite?></td>
				<td><?=$pdtqtt->prixPartiel()?></td>
			</tr>
			<?php
		}
		?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3"> TOTAL :</td>
				<td><?=$mouvement->prixTotal() ?></td>
			</tr>
		</tfoot>
	</table>
</section>

<section class="divers">
	<h3> Commentaire </h3>
	<p><?= $mouvement->commentaire() ?></p>
	<?php if ($mouvement->avancement()->etat=='cree'){ ?>
	<form action="detailsCommande.php" method="POST">
		<input type="hidden" name="mouvement" value="<?= ($compte->type.'|'.$mouvement->ID()) ?>">
		<input type="text" name="commentaire" placeholder="nouveau commentaire">
		<input type="submit" name="submit" value="<?= ($vide?'ajouter un commentaire':'modifier')?>">
	</form>
	<?php }
	?>
</section>
<br><br>

<?php if ($mouvement->avancement()->etat=='cree') {?>
	<form method="POST" action="annulationCommande.php">
		<input type="hidden" name="mouvement" value="<?=$_POST['mouvement'] ?>">
		<input type="submit" name="submit" value="retirer la commande">
	</form>
	<?php
}?>
</body>
</html>
