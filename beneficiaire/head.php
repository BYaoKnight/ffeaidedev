<head>
	<meta charset="utf-8">
	<title><?= $pageTitle ?></title>
	<link rel="stylesheet" type="text/css" href="../style/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../style/<?= (in_array($pageTitle,['login','déconnecté','Erreur Connexion'],true)?'style':'tableauDeBord')?>.css">
	<link rel="icon" type="image/jpg" href="../img/plateau.jpg">
</head>