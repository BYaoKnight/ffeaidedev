<?php
require_once "../backB/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require_once "../loadB/loadBeneficiaire.php";
require_once "../loadB/loadMouvements.php";
$pageTitle='historique des commandes';

?>




<!DOCTYPE HTML>
<html>
<?php require_once "head.php";?>
<body>





<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";
?>


<!-------------------corps du tableau de bord-------------------------------------->
<section>
<?php 

foreach ($_SESSION['beneficiaire']->comptes as $compte) {
	if($compte->type=='aide_developpement'){
	$trclass='alpha';
	$noMove=true;


	echo "<section class=mouvementsCompte>";
	echo "<span class=\"compte\">Compte : ".$compte->type ."</span>";?>
	<table width=100%>
		<tr class=firstRow>
			<th></th>
			<th width="100px">date de valeur</th>
			<th >description</th>
			<th width="125px">état d'avancement</th>
			<th width="80px">montant</th>
			<th> commentaire </th>
		</tr>



<?php //---------------------mouvements---------------------------------------------
	foreach ($compte->mouvements() as /*$moveID =>*/ $move) {
		$etat=$move->avancement()->etat;
		//echo '<tr><td colspan=4 style="text-align:left;">';var_dump($move);echo "</td></tr>";
		$noMove=false;
		$trclass=($trclass=='alpha')?'beta':'alpha';
		?>
		<tr class="<?=$trclass ?>">
			<td>
				<form method="POST" action="detailsCommande.php">
					<input type="hidden" name="mouvement" value="<?=($compte->type.'|'.$move->ID())?>">
					<input type="submit" name="submit" value="voir la commande">
				</form>
			</td>
			<td><?= $move->avancement()->date ?></td>
			<td><?= $move->description() ?></td>
			<td><?= ($etat=='cree'?'en attente':$etat) ?></td>
			<td><?= $move->montant() ?></td>
			<td><?= $move->commentaire() ?></td>
		</tr>
		<?php		
	}
	if($noMove){
		echo '<tr><td colspan=4 style="text-align:center;">vide</td></tr>';
	}
	echo "</table>  </section><br><br>";
}
}
?>
</section>
</body>
</html>
