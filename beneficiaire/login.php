<?php
session_start();
$pageTitle='login';
?>

<!DOCTYPE html>
<html>

<?php
require_once "../beneficiaire/head.php";
?>
<body>
	<main>
		<section class="login">
			<form action="../index.php" method="post">
				<img class="myheader" src="../img/Logo_FFE.jpg" width="280px">
				<h2 style="color: white;">Connexion</h2>
				<?php if($_SESSION['errorTXT']!=''){
					echo '<div class="mdperror">'.$_SESSION['errorTXT'].'</div>';
				}else{echo '<div class=vide><br></div>';}?>
				<input type="text" name="login" placeholder="identifiant"/><br>
				<input type="password" name="mdep" placeholder="mot de passe" value="alpha" /><br>
				<input class="btnSubmit" type="submit" value="Valider" />
			</form>
		</section>
		<section class="image">
			<img src="../img/fond_login.gif">
		</section>
	</main>
	<br><br>
	<section class="message">
		<p>Si vous n'avez pas reçu votre login et votre mot de passe, c'est parce que vous n'êtes pas enregistré comme éligible aux aides au développement.</p>
		<p> Pour remédiez à cette situation rendez-vous dans votre espace club/comité sur <a href="http://clubs.echecs.asso.fr">echecs.asso.fr</a>, cherchez, puis téléchargez, et remplissez le formulaire de candidature aux aides au développement que vous renverrez à l'adresse de la Fédération.</p>
		<p> Si vous avez déjà envoyé le documment et êtes en attente de la réponse, ou pour toute autre demande à ce sujet, vous pouvez vous en enquérir auprès de <a href="mailto:jerome.valenti@ffechecs.fr?Subject=Aide%20D&eacute;veloppement">Jérôme Valenti</a></p>
	</section>
</body>
</html>