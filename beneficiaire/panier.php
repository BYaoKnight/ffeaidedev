<?php 
require_once "../backB/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require "../loadB/loadBeneficiaire.php";
require "../loadB/loadPanier.php";
$pageTitle='Panier';
?>


<!DOCTYPE HTML>
<html>
<?php require_once "head.php";?>
<body>



<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";



//<!--======================= gestion des quantite (0-+) ========================================-->

require_once "../backB/panier.php";





//=================================== affichage du panier ===============================

foreach ($_SESSION['beneficiaire']->comptes as $compte) {
	if($compte->type=='aide_developpement'){
		$solde=$compte->solde();
		$encours=$compte->encours();
		$prixTotal=$compte->mouvements()[0]->prixTotal();

		echo '<section class="panier"><h2>', $compte->type ,'</h2>';?>
		<table>
			<tr>
				<td class="panier" colspan="5">
					solde : <?= $compte->solde().' €'. $compte->printEC() ?>
				</td>
			</tr>
			<tr>
				<th></th>
				<th width="30%">nom</th>
				<th width="90">prix unitaire</th>
				<th width="100">quantité</th>
				<th width="50">montant</th>
			</tr>


		<?php
		//var_dump($compte);
		$vide=true;
		foreach ($compte->mouvements()[0]->produitsQtt() as $pdtqtt) {
			if($pdtqtt->quantite>0){
				echo '
				<tr class="beta">
					<form class="plusOuMoins" action="panier.php" method="post">
					<td>
							<input type="hidden" name="produit" value="', $pdtqtt->produit->ID ,'">
							<input type="submit" name="value" value="0">►
					</td>
					<td>', $pdtqtt->produit->name ,'</td>
					<td>', $pdtqtt->produit->prix ,'</td>
					<td><pre><input type="submit" name="value" value="-">  ',
					 $pdtqtt->quantite ,
					 '  <input type="submit" name="value" value="+"></pre></td>
					<td>', $pdtqtt->prixPartiel() ,'</td>
					</form>
				</tr>';
				$vide=false;
			}
		}
		if($vide){
			echo '<tr><td colspan="5" style="text-align:center;"> vide </td></tr>';
			$c=false;
		}
		else{
			$a=($prixTotal<=$solde);
			$b=($prixTotal<=($solde+$encours));
			$c=($a AND $b AND $prixTotal>'0');
			echo '
				<tr>
					<th colspan="4" align="right">TOTAL</th>
					<th '.($c?'':('style="background-color:red;"')).'>'.
					$prixTotal.'</th>
				</tr>';
		}
		echo '
				<tr>
					<td colspan="4" style="text-align:right">
						<form class="panier" action="catalogue.php" method="post">
							<!-- <input type="hidden" name="catalogue" value="catalogue<?= $_SESSION[\'beneficiaire\']->type().$compte->type ?>"> -->
							<input type="submit" value="catalogue">
						</form>
					</td>
					<td colspan="2" style="text-align:right">
						<form class="panier" action="commande0.php" method="post">
							<input type="hidden" name="compte" value="'.$compte->type.'">
							<input'.($c?'':'disabled="disabled"').' type="submit" value="commander">
						</form>
					</td>
				</tr>	</table>
		</section>';
	}
		//break;
}

