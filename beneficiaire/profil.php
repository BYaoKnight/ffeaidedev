<?php 
require_once "../backB/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require "../loadB/loadBeneficiaire.php";
$beneficiaire=$_SESSION['beneficiaire'];
$pageTitle='Profil';
?>



<!DOCTYPE HTML>
<html>
<?php require_once "head.php";?>
<body>

<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";
?>

<section class="divers">
	<h3> Informations générales </h3>
<pre>
nom du club : <?= $beneficiaire->nom ?> 
identifiant : <?= $beneficiaire->getID() ?> 
Commune     : <?= $beneficiaire->Commune ?> 
mandataire  : <?= $beneficiaire->mandataire ?><form method="POST" action="mdf_Info.php"><input type="text" name="mandataire" placeholder="nom du nouveau mandataire"><input type="submit" name="Smandataire" value="modifier"></form>
email       : <?= $beneficiaire->email ?>
</pre>
</section>
<!--br><br-->
<section class="divers">
	<h3> Comptes </h3>
	<?php 
	foreach ($beneficiaire->comptes as $compte) {
		echo "<div>Compte : ".$compte->type ."<br>solde : ". $compte->solde().' €'. $compte->printEC()."</div><br>";
	}
	?>
</section>
<section class="divers">
	<h3> Adresses </h3>
<?php
	$vide=true;
	foreach ($beneficiaire->adresses as $key => $adresse) {
		$vide=false;?>
		<div class="adresse">
			<h3> <?= $key ?> </h3>
			<p><?= $adresse ?></p>
		</div>
	<?php
	}
	if($vide){
		echo "Vous n'avez aucune adresse enregistrée";
	}
	?><br><br>
	<a href="ajoutAdresse.php"><button type="button">Ajouter une adresse</button></a>
	<a href="Adresses.php"><button type="button">Gérer mes adresses</button></a>
</section>
</body>
</html>
<?php  ?>