<?php

class Adresse {
	public $numero;
	public $voie;
	public $suite;
	public $codePostal;
	public $commune;
	public function __construct($num,$voie,$suite,$CP,$commune){
		$this->voie=$voie;
		$this->numero=$num;
		$this->suite=$suite;
		$this->codePostal=$CP;
		$this->commune=$commune;
	}
	public function __toString(){
		return $this->numero.' '.$this->voie.($this->suite==''?'':'<br>'.$this->suite)
		.'<br>'.$this->codePostal.' '.$this->commune;
	}
}

?>