<?php
abstract class Beneficiaire{

	private $ID;
	public $Commune;
	public $nom;
	public $mandataire;
	public $email;
	public $comptes;
	public $adresses;


	//constuct et destruct---------------------------------
	public function __construct($id,$commune,$nom,$mandataire,$email=''){
		$this->ID=$id;
		$this->Commune=$commune;
		$this->nom=$nom;
		$this->mandataire=$mandataire;
		$this->email=$email;
		$this->comptes=array();
		$this->adresses=array();
	}
	public function __destruct(){}


	//-----------  printing functions  --------------------
	public function __toString(){
		return $this->type().'-'.$this->ID .'-'.$this->Commune.
		'<br>mandataire : '.$this->mandataire();
	}
	public function titre(){
		return /*$this->type().'-'.*/$this->ID .'-'.$this->Commune;
	}


	//---------------- ID  --------------------------------
	public function getID(){return $this->ID;}


	//------------  mandataire  ---------------------------
	public function mandataire(){return $this->mandataire;}


	//-------------  Adresses  ----------------------------
	public function ajoutadresse($adresse,$nom=''){
		$this->adresses[$nom]=$adresse;
	}


	//--------------------  Comptes  ----------------------
	public function ajoutCompte($compte){
		$compte->proprietaire=$this;
		$this->comptes[]=$compte;
	}
	public function nbComptes(){
		return count($this->comptes);
	}
	public function getCompte($type){
		foreach ($this->comptes as $compte) {
			if($compte->type==$type){
				return $compte;
			}
		}
		return NULL;
	}


	public abstract function type();

	//static
	public static $patternID="#^(CJE|[A-Z]\d[A-Z0-9])\d{3}$#";
	public static $patternMDP='#^[!-/:0-9a-z]{5,32}$#i';

}

class Club extends Beneficiaire{
	public function type(){return 'Club';}
}
/*class Comite extends Beneficiaire{
	public function type(){return 'Comité';}
}*/
?>