<?php

class Compte{

	public $type;
	public $soldeInitial;
	public $solde;
	public $encours;
	private $mouvements;
	public $proprietaire;


	//----------  constuct et destruct  -------------------
	public function __construct($type,$soldeInitial=0,$solde=0,$encours=0){
		$this->type=$type;
		$this->soldeInitial=$soldeInitial;
		$this->mouvements=array();
		$this->solde=$solde;
		$this->encours=$encours;
	}
	public function __destruct(){}


	//---------------  mouvements  ------------------------
	public function mouvements(){
		return $this->mouvements;
	}
	public function ajoutMouvement($mouvement){
		$mouvement->compteAssocie=&$this;
		$this->mouvements[]=$mouvement;
	}


	//--------------- Solde  ------------------------------
	public function solde(){
		return $this->solde;
	}
	public function encours(){
		return $this->encours;
	}
	public function printEC(){
		return ($this->encours()==0?"":"<br>après les opérations en attente : ".($this->solde()-$this->encours())." €" );
	}
}
?>