<?php 


class Avancement{
	public $date;
	public $etat;
	public function __construct($date,$etat){
		$this->date=$date;
		$this->etat=$etat;
	}
}


class Mouvement{

	private $ID;
	private $produitsQtt;
	public $avancement;
	public $compteAssocie;
	public $beneficiaire;
	public $adresseLivraison;
	public $commentaire;


	//----------  constuct et destruct---------------------
	public function __construct($ID,$compteID,$beneficiaireID,$commentaire=''){
		$this->ID=$ID;
		$this->produitsQtt=array();
		$this->compteAssocie=$compteID;
		$this->beneficiaire=$beneficiaireID;
		$this->avancement=array();
		$this->adresseLivraison=NULL;
		$this->commentaire=$commentaire;
	}
	public function __destruct(){}


	//------------------  ID  -----------------------------
	public function ID(){return $this->ID;}


	//-----------------  avancement  ----------------------
	public function avancer($avancement){$this->avancement[]=$avancement;}
	public function avancement(){return end($this->avancement);}


	//----------------  adresse  --------------------------
	public function ajoutAdresse($adresse){$this->adresseLivraison=$adresse;}


	//-----------------  produits  ------------------------
	public function produitsQtt(){return $this->produitsQtt;}
	public function produits(){
		$ret=array();
		foreach ($this->produitsQtt as $pdtQtt) {
			$ret[]=$pdtQtt->produit;
		}
		return $ret;
	}
	public function ajouterProduit($produit,$quantite){
		$this->produitsQtt[]=new ProduitQuantite($produit,$quantite);
	}


	// THIS
	public function description(){
		$description=NULL;
		foreach ($this->produitsQtt as $produitQtt) {
			$description=($description==NULL?'':$description.'; ').$produitQtt->quantite.' '.$produitQtt->produit->name;
		}
		return $description;
	}
	public function montant(){
		$total=0;
		foreach ($this->produitsQtt as $produitQtt) {
			$total+=($produitQtt->quantite*$produitQtt->produit->montant());
		}
		return $total;
	}
	public function prixTotal(){
		$total=0;
		foreach ($this->produitsQtt as $pdtqtt) {
			$total+=($pdtqtt->quantite*$pdtqtt->produit->prix);
		}
		return $total;
	}
	public function commentaire(){return $this->commentaire;}
}

?>