<?php

class Produit{

	public $ID;
	public $name;
	public $contenu;
	public $prix;


	public function __construct($ID,$name,$contenu,$prix,$catalogue='aide_developpement',$beneficiaire='Club'){
		$this->ID=$ID;
		$this->name=$name;
		$this->contenu=$contenu;
		$this->prix=$prix;
		self::selfAjoutProduit($catalogue,$beneficiaire,$this);
	}
	public function montant(){
		return ($this->name=='recharge')?$this->prix:-$this->prix;
	}
	public function urlImage($type='v'){
		return '../img/'.$type.$this->name.'.jpg';
	}

	//-------------  catalogues  --------------------------
	public static $catalogues=array();
	public static function produits($catalogue)	{return self::$catalogues[$catalogue];}
	private static function selfAjoutProduit($catalogue,$beneficiaire,$produit){
		if(!isset(self::$catalogues["$beneficiaire:$catalogue"])){
			self::$catalogues["$beneficiaire:$catalogue"]=new Catalogue($beneficiaire.':'.$catalogue);
		}
		self::$catalogues["$beneficiaire:$catalogue"]->produits[]=$produit;
	}
}


class Catalogue{
	public $nom;
	public $produits;
	public function __construct($nom)
	{
		$this->nom=$nom;
		$this->produits=array();
	}
}


class ProduitQuantite{
	public $produit;
	public $quantite;
	
	function __construct($produit,$quantite){
		$this->produit=$produit;
		$this->quantite=$quantite;
	}
	function prixPartiel(){
		return $this->produit->prix * $this->quantite;
	}
}

?>