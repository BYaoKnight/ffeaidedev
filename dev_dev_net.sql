-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : ef7538-001.privatesql
-- Généré le :  lun. 12 août 2019 à 18:18
-- Version du serveur :  5.7.21-log
-- Version de PHP :  5.6.40-0+deb8u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dev_dev_net`
--

-- --------------------------------------------------------

--
-- Structure de la table `Adresse`
--

CREATE TABLE `Adresse` (
  `beneficiaireID` varchar(8) NOT NULL,
  `surnom` varchar(20) NOT NULL,
  `numero` varchar(8) DEFAULT NULL,
  `voie` varchar(40) NOT NULL,
  `suite` varchar(100) DEFAULT NULL,
  `codePostal` varchar(5) NOT NULL,
  `commune` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Avancement`
--

CREATE TABLE `Avancement` (
  `mouvementID` int(11) NOT NULL,
  `compteID` enum('general','aide_developpement','autre') NOT NULL,
  `beneficiaireID` varchar(8) NOT NULL,
  `etat` enum('panier','cree','valide','refuse') NOT NULL DEFAULT 'panier',
  `dateDeValeur` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Beneficiaire`
--

CREATE TABLE `Beneficiaire` (
  `ID` varchar(8) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `Commune` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `mandataire` varchar(40) DEFAULT NULL,
  `mdp` varchar(33) DEFAULT 'alpha'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `Beneficiaire`
--
DELIMITER $$
CREATE TRIGGER `Beneficiaire_ai_CreationCompteAideDev` AFTER INSERT ON `Beneficiaire` FOR EACH ROW INSERT INTO Compte VALUES ('aide_developpement',NEW.ID,200)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `catalogueClubaide_developpement`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `catalogueClubaide_developpement` (
`ID` varchar(20)
,`nom` varchar(50)
,`contenu` varchar(100)
,`prix` int(11)
,`fournisseurID` int(11)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `clubs_inactifs`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `clubs_inactifs` (
`ID` varchar(8)
,`nom` varchar(50)
,`Commune` varchar(30)
,`email` varchar(100)
,`mandataire` varchar(40)
,`mdp` varchar(33)
);

-- --------------------------------------------------------

--
-- Structure de la table `Compte`
--

CREATE TABLE `Compte` (
  `nom` enum('general','aide_developpement','autre') NOT NULL,
  `proprietaireID` varchar(8) NOT NULL,
  `soldeInitial` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `Compte`
--
DELIMITER $$
CREATE TRIGGER `Compte_ai_CreationPanier` AFTER INSERT ON `Compte` FOR EACH ROW INSERT INTO Mouvement VALUES (0,NEW.nom,NEW.proprietaireID, DEFAULT,DEFAULT)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Contenu`
--

CREATE TABLE `Contenu` (
  `mouvementID` int(11) NOT NULL,
  `compteID` enum('general','aide_developpement','autre') NOT NULL,
  `beneficiaireID` varchar(8) NOT NULL,
  `produit` varchar(20) NOT NULL,
  `quantite` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Fournisseur`
--

CREATE TABLE `Fournisseur` (
  `ID` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `RIB` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `logins`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `logins` (
`ID` varchar(8)
,`mdp` varchar(33)
);

-- --------------------------------------------------------

--
-- Structure de la table `log_Connexion`
--

CREATE TABLE `log_Connexion` (
  `beneficiaireID` varchar(8) NOT NULL,
  `horaire` datetime NOT NULL,
  `Fin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Mouvement`
--

CREATE TABLE `Mouvement` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `compteID` enum('general','aide_developpement','autre') NOT NULL,
  `beneficiaireID` varchar(8) NOT NULL,
  `adresseNom` varchar(20) DEFAULT NULL,
  `commentaire` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `Mouvement`
--
DELIMITER $$
CREATE TRIGGER `Mouvement_ai_CreationAvancementPanier` AFTER INSERT ON `Mouvement` FOR EACH ROW INSERT INTO Avancement 
	VALUES(NEW.ID,           
           NEW.CompteID,
           NEW.beneficiaireID,
           'panier',
           NULL)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Mouvement_bu_Commander` BEFORE UPDATE ON `Mouvement` FOR EACH ROW IF OLD.ID<>NEW.ID THEN
   UPDATE Avancement SET etat='cree', dateDeValeur=CURRENT_DATE()
   WHERE Avancement.mouvementID=OLD.ID AND Avancement.compteID=NEW.compteID AND Avancement.beneficiaireID=NEW.beneficiaireID;
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `Mvt_1avancement_produits_adresse`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `Mvt_1avancement_produits_adresse` (
`mvtID` int(11)
,`compte` enum('general','aide_developpement','autre')
,`beneficiaire` varchar(8)
,`etat` enum('panier','cree','valide','refuse')
,`dateDeValeur` date
,`surnom` varchar(20)
,`numero` varchar(8)
,`voie` varchar(40)
,`suite` varchar(100)
,`codePostal` varchar(5)
,`commune` varchar(50)
,`quantite` int(3)
,`nom` varchar(50)
,`prix` int(11)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `Mvt_Adresse`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `Mvt_Adresse` (
`mvtID` int(11)
,`compte` enum('general','aide_developpement','autre')
,`beneficiaire` varchar(8)
,`commentaire` varchar(200)
,`surnom` varchar(20)
,`numero` varchar(8)
,`voie` varchar(40)
,`suite` varchar(100)
,`codePostal` varchar(5)
,`commune` varchar(50)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `Mvt_view`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `Mvt_view` (
`mvtID` int(11)
,`compte` enum('general','aide_developpement','autre')
,`beneficiaire` varchar(8)
,`etat` enum('panier','cree','valide','refuse')
,`dateDeValeur` date
,`surnom` varchar(20)
,`numero` varchar(8)
,`voie` varchar(40)
,`suite` varchar(100)
,`codePostal` varchar(5)
,`commune` varchar(50)
,`quantite` int(3)
,`nom` varchar(50)
,`prix` int(11)
);

-- --------------------------------------------------------

--
-- Structure de la table `Produit`
--

CREATE TABLE `Produit` (
  `ID` varchar(20) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `contenu` varchar(100) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `destinataire` enum('club','comite') DEFAULT NULL,
  `Catalogue` enum('general','aide_developpement','autre') DEFAULT NULL,
  `fournisseurID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `ProduitQtt`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `ProduitQtt` (
`mvtID` int(11)
,`compteID` enum('general','aide_developpement','autre')
,`benefID` varchar(8)
,`quantite` int(3)
,`ID` varchar(20)
,`nom` varchar(50)
,`contenu` varchar(100)
,`prix` int(11)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `WaitingMoves`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `WaitingMoves` (
`beneficiaire` varchar(8)
,`compte` enum('general','aide_developpement','autre')
,`mvtID` int(11)
,`dateDeValeur` date
,`commentaire` varchar(200)
);

-- --------------------------------------------------------

--
-- Structure de la vue `catalogueClubaide_developpement`
--
DROP TABLE IF EXISTS `catalogueClubaide_developpement`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `catalogueClubaide_developpement`  AS  (select `Produit`.`ID` AS `ID`,`Produit`.`nom` AS `nom`,`Produit`.`contenu` AS `contenu`,`Produit`.`prix` AS `prix`,`Produit`.`fournisseurID` AS `fournisseurID` from `Produit` where ((`Produit`.`Catalogue` = 'aide_developpement') and (`Produit`.`destinataire` = 'club'))) ;

-- --------------------------------------------------------

--
-- Structure de la vue `clubs_inactifs`
--
DROP TABLE IF EXISTS `clubs_inactifs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `clubs_inactifs`  AS  select `Beneficiaire`.`ID` AS `ID`,`Beneficiaire`.`nom` AS `nom`,`Beneficiaire`.`Commune` AS `Commune`,`Beneficiaire`.`email` AS `email`,`Beneficiaire`.`mandataire` AS `mandataire`,`Beneficiaire`.`mdp` AS `mdp` from `Beneficiaire` where ((not(`Beneficiaire`.`ID` in (select distinct `log_Connexion`.`beneficiaireID` from `log_Connexion`))) and (not(`Beneficiaire`.`ID` in (select `Mouvement`.`beneficiaireID` from `Mouvement` where (`Mouvement`.`ID` <> 0))))) ;

-- --------------------------------------------------------

--
-- Structure de la vue `logins`
--
DROP TABLE IF EXISTS `logins`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `logins`  AS  select `Beneficiaire`.`ID` AS `ID`,`Beneficiaire`.`mdp` AS `mdp` from `Beneficiaire` ;

-- --------------------------------------------------------

--
-- Structure de la vue `Mvt_1avancement_produits_adresse`
--
DROP TABLE IF EXISTS `Mvt_1avancement_produits_adresse`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `Mvt_1avancement_produits_adresse`  AS  select `Mouvement`.`ID` AS `mvtID`,`Mouvement`.`compteID` AS `compte`,`Mouvement`.`beneficiaireID` AS `beneficiaire`,`Avancement`.`etat` AS `etat`,`Avancement`.`dateDeValeur` AS `dateDeValeur`,`Adresse`.`surnom` AS `surnom`,`Adresse`.`numero` AS `numero`,`Adresse`.`voie` AS `voie`,`Adresse`.`suite` AS `suite`,`Adresse`.`codePostal` AS `codePostal`,`Adresse`.`commune` AS `commune`,`Contenu`.`quantite` AS `quantite`,`Produit`.`nom` AS `nom`,`Produit`.`prix` AS `prix` from ((((`Mouvement` left join `Avancement` on(((`Mouvement`.`ID` = `Avancement`.`mouvementID`) and (`Mouvement`.`compteID` = `Avancement`.`compteID`) and (`Mouvement`.`beneficiaireID` = `Avancement`.`beneficiaireID`)))) left join `Adresse` on(((`Mouvement`.`beneficiaireID` = `Adresse`.`beneficiaireID`) and (`Mouvement`.`adresseNom` = `Adresse`.`surnom`)))) left join `Contenu` on(((`Mouvement`.`ID` = `Contenu`.`mouvementID`) and (`Mouvement`.`compteID` = `Contenu`.`compteID`) and (`Mouvement`.`beneficiaireID` = `Contenu`.`beneficiaireID`)))) left join `Produit` on((`Produit`.`ID` = `Contenu`.`produit`))) where (`Avancement`.`etat` = ifnull((select `A1`.`etat` from `Avancement` `A1` where ((`A1`.`etat` <> 'cree') and (`A1`.`mouvementID` = `Mouvement`.`ID`) and (`A1`.`compteID` = `Mouvement`.`compteID`) and (`A1`.`beneficiaireID` = `Mouvement`.`beneficiaireID`))),'cree')) order by `Mouvement`.`beneficiaireID`,`Mouvement`.`compteID`,`Mouvement`.`ID` ;

-- --------------------------------------------------------

--
-- Structure de la vue `Mvt_Adresse`
--
DROP TABLE IF EXISTS `Mvt_Adresse`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `Mvt_Adresse`  AS  select `Mouvement`.`ID` AS `mvtID`,`Mouvement`.`compteID` AS `compte`,`Mouvement`.`beneficiaireID` AS `beneficiaire`,`Mouvement`.`commentaire` AS `commentaire`,`Adresse`.`surnom` AS `surnom`,`Adresse`.`numero` AS `numero`,`Adresse`.`voie` AS `voie`,`Adresse`.`suite` AS `suite`,`Adresse`.`codePostal` AS `codePostal`,`Adresse`.`commune` AS `commune` from (`Mouvement` left join `Adresse` on(((`Mouvement`.`beneficiaireID` = `Adresse`.`beneficiaireID`) and (`Mouvement`.`adresseNom` = `Adresse`.`surnom`)))) where (`Mouvement`.`ID` <> 0) order by `Mouvement`.`beneficiaireID`,`Mouvement`.`compteID`,`Mouvement`.`ID` ;

-- --------------------------------------------------------

--
-- Structure de la vue `Mvt_view`
--
DROP TABLE IF EXISTS `Mvt_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `Mvt_view`  AS  select `Mouvement`.`ID` AS `mvtID`,`Mouvement`.`compteID` AS `compte`,`Mouvement`.`beneficiaireID` AS `beneficiaire`,`Avancement`.`etat` AS `etat`,`Avancement`.`dateDeValeur` AS `dateDeValeur`,`Adresse`.`surnom` AS `surnom`,`Adresse`.`numero` AS `numero`,`Adresse`.`voie` AS `voie`,`Adresse`.`suite` AS `suite`,`Adresse`.`codePostal` AS `codePostal`,`Adresse`.`commune` AS `commune`,`Contenu`.`quantite` AS `quantite`,`Produit`.`nom` AS `nom`,`Produit`.`prix` AS `prix` from ((((`Mouvement` left join `Avancement` on(((`Mouvement`.`ID` = `Avancement`.`mouvementID`) and (`Mouvement`.`compteID` = `Avancement`.`compteID`) and (`Mouvement`.`beneficiaireID` = `Avancement`.`beneficiaireID`)))) left join `Adresse` on(((`Mouvement`.`beneficiaireID` = `Adresse`.`beneficiaireID`) and (`Mouvement`.`adresseNom` = `Adresse`.`surnom`)))) left join `Contenu` on(((`Mouvement`.`ID` = `Contenu`.`mouvementID`) and (`Mouvement`.`compteID` = `Contenu`.`compteID`) and (`Mouvement`.`beneficiaireID` = `Contenu`.`beneficiaireID`)))) left join `Produit` on((`Produit`.`ID` = `Contenu`.`produit`))) order by `Mouvement`.`beneficiaireID`,`Mouvement`.`compteID`,`Mouvement`.`ID`,`Avancement`.`etat` ;

-- --------------------------------------------------------

--
-- Structure de la vue `ProduitQtt`
--
DROP TABLE IF EXISTS `ProduitQtt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `ProduitQtt`  AS  select `Contenu`.`mouvementID` AS `mvtID`,`Contenu`.`compteID` AS `compteID`,`Contenu`.`beneficiaireID` AS `benefID`,`Contenu`.`quantite` AS `quantite`,`Produit`.`ID` AS `ID`,`Produit`.`nom` AS `nom`,`Produit`.`contenu` AS `contenu`,`Produit`.`prix` AS `prix` from (`Contenu` join `Produit` on((`Produit`.`ID` = `Contenu`.`produit`))) order by `Contenu`.`beneficiaireID`,`Contenu`.`compteID`,`Contenu`.`mouvementID` ;

-- --------------------------------------------------------

--
-- Structure de la vue `WaitingMoves`
--
DROP TABLE IF EXISTS `WaitingMoves`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev_dev_net`@`%` SQL SECURITY DEFINER VIEW `WaitingMoves`  AS  select `Mouvement`.`beneficiaireID` AS `beneficiaire`,`Mouvement`.`compteID` AS `compte`,`Mouvement`.`ID` AS `mvtID`,`A1`.`dateDeValeur` AS `dateDeValeur`,`Mouvement`.`commentaire` AS `commentaire` from (`Mouvement` join `Avancement` `A1` on(((`A1`.`mouvementID` = `Mouvement`.`ID`) and (`A1`.`compteID` = `Mouvement`.`compteID`) and (`A1`.`beneficiaireID` = `Mouvement`.`beneficiaireID`)))) where ((`A1`.`etat` = 'cree') and (not(`Mouvement`.`ID` in (select `A2`.`mouvementID` from `Avancement` `A2` where ((`A2`.`etat` <> 'cree') and (`A2`.`compteID` = `A1`.`compteID`) and (`A2`.`beneficiaireID` = `A1`.`beneficiaireID`)))))) order by `A1`.`dateDeValeur` desc ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Adresse`
--
ALTER TABLE `Adresse`
  ADD PRIMARY KEY (`beneficiaireID`,`surnom`);

--
-- Index pour la table `Avancement`
--
ALTER TABLE `Avancement`
  ADD PRIMARY KEY (`mouvementID`,`compteID`,`beneficiaireID`,`etat`),
  ADD KEY `Avancement_ibfk_1` (`beneficiaireID`,`compteID`,`mouvementID`);

--
-- Index pour la table `Beneficiaire`
--
ALTER TABLE `Beneficiaire`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `Compte`
--
ALTER TABLE `Compte`
  ADD PRIMARY KEY (`nom`,`proprietaireID`),
  ADD KEY `Compte_ibfk1` (`proprietaireID`);

--
-- Index pour la table `Contenu`
--
ALTER TABLE `Contenu`
  ADD PRIMARY KEY (`mouvementID`,`compteID`,`beneficiaireID`,`produit`),
  ADD KEY `produit` (`produit`);

--
-- Index pour la table `Fournisseur`
--
ALTER TABLE `Fournisseur`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `log_Connexion`
--
ALTER TABLE `log_Connexion`
  ADD PRIMARY KEY (`beneficiaireID`,`horaire`);

--
-- Index pour la table `Mouvement`
--
ALTER TABLE `Mouvement`
  ADD PRIMARY KEY (`ID`,`compteID`,`beneficiaireID`),
  ADD KEY `compteID` (`compteID`,`beneficiaireID`),
  ADD KEY `adresseID` (`adresseNom`),
  ADD KEY `Mouvement_ibfk_1` (`beneficiaireID`,`compteID`),
  ADD KEY `Mouvement_ibfk_2` (`beneficiaireID`,`adresseNom`);

--
-- Index pour la table `Produit`
--
ALTER TABLE `Produit`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fournisseurID` (`fournisseurID`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Adresse`
--
ALTER TABLE `Adresse`
  ADD CONSTRAINT `Adresse_ibfk_1` FOREIGN KEY (`beneficiaireID`) REFERENCES `Beneficiaire` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Avancement`
--
ALTER TABLE `Avancement`
  ADD CONSTRAINT `Avancement_ibfk_1` FOREIGN KEY (`beneficiaireID`,`compteID`,`mouvementID`) REFERENCES `Mouvement` (`beneficiaireID`, `compteID`, `ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Compte`
--
ALTER TABLE `Compte`
  ADD CONSTRAINT `Compte_ibfk1` FOREIGN KEY (`proprietaireID`) REFERENCES `Beneficiaire` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Contenu`
--
ALTER TABLE `Contenu`
  ADD CONSTRAINT `Contenu_ibfk_1` FOREIGN KEY (`mouvementID`,`compteID`,`beneficiaireID`) REFERENCES `Mouvement` (`ID`, `compteID`, `beneficiaireID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Contenu_ibfk_2` FOREIGN KEY (`produit`) REFERENCES `Produit` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `log_Connexion`
--
ALTER TABLE `log_Connexion`
  ADD CONSTRAINT `log_Connexion_ibfk_1` FOREIGN KEY (`beneficiaireID`) REFERENCES `Beneficiaire` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Mouvement`
--
ALTER TABLE `Mouvement`
  ADD CONSTRAINT `Mouvement_ibfk_1` FOREIGN KEY (`beneficiaireID`,`compteID`) REFERENCES `Compte` (`proprietaireID`, `nom`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Mouvement_ibfk_2` FOREIGN KEY (`beneficiaireID`,`adresseNom`) REFERENCES `Adresse` (`beneficiaireID`, `surnom`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Produit`
--
ALTER TABLE `Produit`
  ADD CONSTRAINT `Produit_ibfk_1` FOREIGN KEY (`fournisseurID`) REFERENCES `Fournisseur` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
