<?php
require_once "../backG/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
require_once "../backG/load.php";
require_once "../classes/Constantes.php";
$pageTitle='Ajout de beneficiaires';
//=========================  functions  ===========================================================
	function toCSV($array,$char=','){
		$ret="";
		if (isset($array[0])) {
			$ret.=$array[0];
			$i=1;
			while(isset($array[$i])){
				$ret.=$char.$array[$i];
				$i++;
			}
		}
		return $ret;
	}
	function createMDP(){
		$ret=array('','');
		$max=rand(8,12);
		$i=0;
		while ($i < 12) {
			$c=chr(rand(1,26)+32*rand(1,3));
			$ret[1].=$c;
			$ret[0].=($c=="'"?"\'":$c);
			$i++;
		}
		return $ret;
	}

?>
<!DOCTYPE HTML>
<html>
<?php require_once "../beneficiaire/head.php";?>
<body>
<?php require_once "header.php";
//echo MAIL_GESTIONNAIRE."<br><br><br>".MAIL_HEADERS."<br><br>";
//echo "<pre>";
$color=' style="color:red"';
if (!empty($_POST)) {
	//var_dump($_POST);

	$msg='';
	$InsertionOK=true;
	$beneficiairesInfo=array();
	$trans=array(0=>'id',1=>'nom',2=>'Commune',3=>'email',4=>'mandataire');
	//prepare data
	if(isset($_POST['id']) AND isset($_POST['nom']) AND isset($_POST['email']) AND isset($_POST['mandataire'])){
		//echo "solo";
		$temp['id']=$_POST['id'];
		$temp['nom']=$_POST['nom'];
		$temp['Commune']=(isset($_POST['Commune'])?$_POST['Commune']:'');
		$temp['email']=$_POST['email'];
		$temp['mandataire']=$_POST['mandataire'];
		foreach ($temp as $key2 => &$value2) {
			$temp2=preg_split("# #", $value2,-1, PREG_SPLIT_NO_EMPTY);
			//echo "\n\ntmp2\n";
			//var_dump($temp2);
			if(empty($temp2)){
				$value2='';
			}
			else{
				$value2=toCSV($temp2,' ');
			}
		}
		$beneficiairesInfo[]=$temp;
	}
	//echo "<pre>";
	if(isset($_POST['beneficiaires'])){
		//echo "multi";
		$beneficiaires=preg_split("#(\r\n|\r|\n)#", $_POST['beneficiaires'],-1, PREG_SPLIT_NO_EMPTY);
		//var_dump($beneficiaires);
		foreach ($beneficiaires as $key => $benefi) {
			$temp=array();
			$temp1=preg_split("#,#", $benefi/*,-1, PREG_SPLIT_NO_EMPTY*/);
			$max=count($temp1);
			for ($i=0; $i < $max ; $i++) { 
				$temp[$trans[$i]]=$temp1[$i];
			}
			foreach ($temp as $key2 => &$value2) {
				$temp2=preg_split("# #", $value2,-1, PREG_SPLIT_NO_EMPTY);
				//echo "\n\ntmp2\n";var_dump($temp2);
				if(empty($temp2)){
					$value2='';
				}
				else{
					$value2=toCSV($temp2,' ');
				}
				$value2=preg_replace("#'#", "\\'", $value2);
			}
			//echo "\n\ntmp\n";var_dump($temp);
			$tempo=new ArrayObject($temp);
			$beneficiairesInfo[]=$tempo->getArrayCopy();
		}
	}
	//var_dump($beneficiairesInfo);
	//analyse and insert data
	//echo "\n\nanalyse and insert data\n\n";
	if(!empty($beneficiairesInfo)){
		//var_dump($beneficiairesInfo);
		foreach ($beneficiairesInfo as $key => $benef) {
			//var_dump($benef);
			$pb=array();
			$benef['email']=strtolower($benef['email']);
			$ligne="ligne $key (".$benef['id'].",".$benef['nom'].",".$benef['Commune'].",".$benef['email'].",".$benef['mandataire'].") ";
			if (!preg_match(Beneficiaire::$patternID, $benef['id'])) {$pb[]='identifiant';}
			if(strlen($benef['nom'])>50){$benef['nom']=substr($benef['nom'],0,50);}
			if(strlen($benef['Commune'])>30){$benef['Commune']=substr($benef['Commune'],0,30);}
			if(strlen($benef['email'])>100 OR !preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#",$benef['email'])){$pb[]='email';}
			if(strlen($benef['mandataire'])>40){$benef['mandataire']=substr($benef['mandataire'],0,20);}
			if(!preg_match("#[a-zâàäéêèëîïôöûüç' -]#i", $benef['mandataire'])){$pb[]='mandataire';}
			
			if(count($pb)){
				$ligne.=": le beneficiaire n'a pas pu être enregistré car des informations saisies ne correspondent pas aux critères (".toCSV($pb).").";
				$InsertionOK=false;
			}
			else{
				$mdep= createMDP();
				$mdpsql=$mdep[0];
				$mdp=$mdep[1];
				$ligne.=": les informations du beneficiaire sont correctes.\r\n";
				$sql=" (".toCSV(array("'".$benef['id']."'","'".$benef['nom']."'","'".$benef['Commune']."'",  "'".$benef['email']."'","'".$benef['mandataire']."'","'".$mdpsql."'")).")";
				$ligne.="Insertion dans la base de données ";
				//var_dump($sql);
				$result=$bdd->query("INSERT into Beneficiaire Values ".$sql);
				$InsertionOK=$result?true:false;
				$ligne.='  '.($InsertionOK?'réussie':'échoué')."\r\n".$sql;
				//var_dump($result);
				if ($InsertionOK) {
					$ligne.="\r\nLe mail ";

					$corpsMail="<html><body><pre>";
					$corpsMail.="\tBonjour ".$benef['mandataire'].",\n";
					$corpsMail.="après étude de votre projet dans le cadre de l'aide aux clubs (annoncé par le courrier du 14 Juin de M. VALENTI Jérôme), la FFE a le plaisir de vous informer qu'il a été accepté.\r\nVous pouvez dès à présent bénéficier de l'aide en vous connectant à la plateforme logistique disponible sur <a href=\"http://developpement.ffechecs.net\" >developpement.ffechecs.net</a> avec les identifiants suivants :\r\n - login : ".$benef['id']."\r\n - mot de passe : ".$mdp."\r\n\r\n";
					$corpsMail.="Nous vous souhaitons beaucoup de réussite dans votre projet\r\nCordialement\r\nLa FFE</pre></body></html>";
					if(mail($benef['email'],'FFE: Projet aide aux clubs',$corpsMail,MAIL_HEADERS)){
						$ligne.="a bien été";}
					else{$ligne.="n'a pas pu être";$InsertionOK=false;}
					$ligne.=" envoyé.";
				}
			}
			$msg.='<span '.($InsertionOK?'':$color).'>'.$ligne."</span>\r\n\r\n";
		}
	}
	//echo $msg."fin msg\n\n ";
	//=========================================  mail  ================================================
		
		$corpsMail="<html><body><pre>\tBonjour,\r\nvoici le compte rendu du dernier essai d'ajout de bénéficiaires : \r\n\r\n";
		$corpsMail.=$msg."</pre></body></html>";
		mail(MAIL_GESTIONNAIRE, 'FFE_aideDev: Ajout de bénéficiaires', $corpsMail,MAIL_HEADERS);

	//	echo "</pre>";
}
//</pre>
/*
bonjour...,
après étude de votre projet dans le cadre de l'aide au club (annoncé par le courrier du 14 Juin de M. VALENTI Jérôme), La FFéchecs a le plaisir de vous informer qu'il a été accepté. Vous pouvez dès à présent bénéficir de l'aide au club en vous connectant à la plateforme logistique disponible sur <a href=\"http://developpement.ffechecs.net\" >developpement.ffechecs.net</a> avec les identifiants suivants :

Nous vous souhaitons beaucoup de réussite dans votre projet
Cordialement
La FFéchecs
*/
?>
<br><br>
<!--------------------------header-------------------------------------------------
CJE011,club du pré vert,,testFFE2019@gmail.com,albert de la cohane
-->



<details class="ajouts">
	<summary><strong> Ajouter plusieurs bénéficiarires en un seul coup</strong></summary>
	<table class="ajoutBenef">
		<tr>
			<th>identifiant </th>
			<th> , </th>
			<th> nom complet </th>
			<th> , </th>
			<th> Commune </th>
			<th> , </th>
			<th> email </th>
			<th> , </th>
			<th> mandataire </th>
		</tr>
	</table>
	<form id="first" action="../gestionnaire/ajoutBeneficiaire.php" method="POST">
		<textarea form="first" rows="20" name="beneficiaires" required><?= $_POST['beneficiaires'] ?></textarea><br>
		<input type="reset" name="reset" value="effacer">
		<input type="submit" name="submit" value="ajouter les bénéficiaires">
	</form>
</details>
<br><br>
<details class="ajout">
	<summary><strong> Ajouter un bénéficiaire à la fois</strong></summary>
	<form action="../gestionnaire/ajoutBeneficiaire.php" method="POST">
		<pre>
 identifiant : <input type="text" name="id" value="<?= $_POST['id'] ?>" required>
 nom complet : <input type="text" name="nom" value="<?= $_POST['nom'] ?>" required>
   Commune   : <input type="text" name="Commune" value="<?= $_POST['Commune'] ?>"  required>
    email    : <input type="text" name="email" value="<?= $_POST['email'] ?>" required>
  mandataire : <input type="text" name="mandataire" value="<?= $_POST['mandataire'] ?>"  required>
		</pre>
		<br>
		<input type="reset" name="reset" value="effacer">
		<input type="submit" name="submit" value="ajouter le bénéficiaire">
	</form>
</details>
</body>
</html>
