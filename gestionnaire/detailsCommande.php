<?php // requrire needed scripts and class declaration
	require_once "../backG/CheckConnection.php";
	require_once "../back/connexionBD.php";
	require_once "../back/classes1.php";
	//require_once "../loadB/loadBeneficiaire.php";
	//require_once "../loadB/.php";
$pageTitle='Détails Commande';
if(!$_POST OR !isset($_POST['data'])){
	header('Location: historiquecmd.php');
	exit();
}
//echo $_POST['data'];
$unchain=preg_split("#[/]#", $_POST['data']);
$benefID=$unchain[0];
$compteID=$unchain[1];
$mvtID=$unchain[2];
$mouvement=new Mouvement($mvtID,$compteID,$benefID);

//loadbeneficiaire

$result=$bdd->query("SELECT * FROM Mvt_Adresse WHERE mvtID='$mvtID' AND compte='$compteID' AND beneficiaire='$benefID';");
//echo "<pre>";var_dump($result);echo "</pre>";
while ($rslt=$result->fetch()) {
	$mouvement->ajoutAdresse(new Adresse($rslt['numero'],$rslt['voie'],$rslt['suite'],$rslt['codePostal'],$rslt['commune']));
	$mouvement->commentaire=$rslt['commentaire'];
	//var_dump($mouvement);
}

$result=$bdd->query("SELECT * FROM Avancement WHERE mouvementID='$mvtID' AND compteID='$compteID' AND beneficiaireID='$benefID' ORDER BY etat;");
while ($rslt=$result->fetch()) {
	$mouvement->avancer(new Avancement($rslt['dateDeValeur'],$rslt['etat']));
}

$result=$bdd->query("SELECT * FROM ProduitQtt WHERE mvtID='$mvtID' AND compteID='$compteID' AND benefID='$benefID';");
while ($rslt=$result->fetch()) {
	$mouvement->ajouterProduit(new Produit($rslt['ID'],$rslt['nom'],$rslt['contenu'],$rslt['prix']),$rslt['quantite']);
}

//------------------------------new beneficiaire-----------------------------------------
	$result=$bdd->query('SELECT * from Beneficiaire WHERE ID=\''.$benefID.'\';');
	//if(!$result){goto END;}
	$beneficiaireInfo=$result->fetch();
	$beneficiaire=new Club($benefID,$beneficiaireInfo['Commune'],$beneficiaireInfo['nom'],$beneficiaireInfo['mandataire'],$beneficiaireInfo['email']);


	//--------------------------------new Comptes--------------------------------------------
	$sql='SELECT DISTINCT nom, soldeInitial FROM Compte WHERE proprietaireID=\''.$benefID.'\' ;';
	$result=$bdd->query($sql);
	//var_dump($sql);
	while($comptes=$result->fetch()){
		$beneficiaire->ajoutCompte(new Compte($comptes['nom'],$comptes['soldeInitial']));
	}
	foreach ($beneficiaire->comptes as $key => &$compte) {
		if ($compte->type==$compteID) {
			$sql0="SELECT SUM(prix*quantite) as depense FROM Contenu 
			JOIN Produit ON produit=Produit.ID
			JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
			WHERE etat='valide' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$benefID."';";

			$sql1="SELECT SUM(prix*quantite) as encours FROM Contenu 
			JOIN Produit ON produit=Produit.ID
			JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
			WHERE etat='cree' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$benefID."' AND Contenu.mouvementID NOT IN (SELECT mouvementID FROM Avancement as A1 WHERE A1.beneficiaireID='".$benefID."' AND A1.compteID='".$compte->type."' AND etat<>'cree');";
			//echo "<pre>";
			$result=$bdd->query($sql0);
			$fetch=$result->fetch();
			$compte->solde=$compte->soldeInitial-(empty($fetch['depense'])?0:$fetch['depense']);

			$result=$bdd->query($sql1);
			$fetch=$result->fetch();
			$compte->encours=(empty($fetch['encours'])?0:$fetch['encours']);
			//var_dump($compte);
			//echo"</pre>";
		}
	}


?>
<!DOCTYPE HTML>
<html>
	<?php require_once "../beneficiaire/head.php";?>
<body>
	<?php require_once "header.php";?>


<!-------------------corps du tableau de bord-------------------------------------->
<!-- beneficiaire -->
	<section class="divers">
		<h2>Beneficiare</h2>
		<!--?= ?-->
		<h3> Informations générales </h3>
			nom du club : <?= $beneficiaire->nom ?> <br>
			identifiant : <?= $beneficiaire->getID() ?> <br>
			Commune     : <?= $beneficiaire->Commune ?> <br>
			mandataire  : <?= $beneficiaire->mandataire ?> <br>
			email       : <?= $beneficiaire->email ?><br>
		<h3> Comptes </h3>
		<?php 
		foreach ($beneficiaire->comptes as $compte) {
			echo "<div>Compte : ".$compte->type ."<br>solde : ". $compte->solde().' €'. $compte->printEC()."</div><br>";
		}
		?>
	</section>
<!-- Adresse -->
	<section class="divers">
		<h3>Adresse de livraison</h3>
		<?=$mouvement->adresseLivraison ?>
	</section>
<!-- Avancement -->
	<section class="divers">
		<h3>Etat d'avancement</h3>
		<table>
			<thead>
				<tr>
					<th> état </th>
					<th> date </th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($mouvement->avancement as $key => $avnct) {?>
				<tr>
					<td> <?=$avnct->etat ?> </td>
					<td> <?=$avnct->date ?> </td>
				</tr>
				<?php
			}?>
			</tbody>
		</table>
	</section>
<!-- Contenu -->
	<section class="divers">
		<h3>Contenu</h3>
		<table>
			<thead>
				<tr>
					<th> Produit </th>
					<th> Prix Unitaire</th>
					<th> Quantité </th>
					<th> montant </th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ($mouvement->produitsQtt() as $key => $pdtqtt) {
				?>
				<tr>
					<td><?=$pdtqtt->produit->name?></td>
					<td><?=$pdtqtt->produit->prix?></td>
					<td><?=$pdtqtt->quantite?></td>
					<td><?=$pdtqtt->prixPartiel()?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"> TOTAL :</td>
					<td><?=$mouvement->prixTotal() ?></td>
				</tr>
			</tfoot>
		</table>
	</section>
<!-- Commentaire -->
	<section class="divers">
		<h3> Commentaire </h3>
		<p><?= $mouvement->commentaire() ?></p>
	</section>
<br><br>
	<form action="../backG/gestionMvt.php" method="POST">
			<input type="hidden" name="data" value="<?=$_POST['data'] ?>">
			<input type="submit" name="submit" value="refuser">
			<input type="submit" name="submit" value="valider">
	</form>
</body>
</html>
