<?php 
require_once "../backG/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";

if (!isset($_POST['club'])) {
	$open=' open';
	$pageTitle='Profil de beneficiaire';
}
else{
	//------------------------------new beneficiaire-----------------------------------------
	$result=$bdd->query('SELECT * from Beneficiaire WHERE ID=\''.$_POST['club'].'\';');
	//if(!$result){goto END;}
	$beneficiaireInfo=$result->fetch();
	$beneficiaire=new Club($_POST['club'],$beneficiaireInfo['Commune'],$beneficiaireInfo['nom'],$beneficiaireInfo['mandataire'],$beneficiaireInfo['email']);


	//--------------------------------new Comptes--------------------------------------------
	$sql='SELECT DISTINCT nom, soldeInitial FROM Compte WHERE proprietaireID=\''.$_POST['club'].'\' ;';
	$result=$bdd->query($sql);
	//var_dump($sql);
	while($comptes=$result->fetch()){
		$beneficiaire->ajoutCompte(new Compte($comptes['nom'],$comptes['soldeInitial']));
	}
	foreach ($beneficiaire->comptes as $key => &$compte) {
		$sql0="SELECT SUM(prix*quantite) as depense FROM Contenu 
		JOIN Produit ON produit=Produit.ID
		JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
		WHERE etat='valide' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$_POST['club']."';";

		$sql1="SELECT SUM(prix*quantite) as encours FROM Contenu 
		JOIN Produit ON produit=Produit.ID
		JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
		WHERE etat='cree' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$_POST['club']."' AND Contenu.mouvementID NOT IN (SELECT mouvementID FROM Avancement as A1 WHERE A1.beneficiaireID='".$_POST['club']."' AND A1.compteID='".$compte->type."' AND etat<>'cree');";
		//echo "<pre>";
		$result=$bdd->query($sql0);
		$fetch=$result->fetch();
		$compte->solde=$compte->soldeInitial-(empty($fetch['depense'])?0:$fetch['depense']);

		$result=$bdd->query($sql1);
		$fetch=$result->fetch();
		$compte->encours=(empty($fetch['encours'])?0:$fetch['encours']);
		//var_dump($compte);
		//echo"</pre>";
	}





	//=====================================  new adresse  ====================================
	$result=$bdd->query('SELECT `numero`,`voie`,`suite`,`codePostal`,`commune`,`surnom` FROM Adresse  WHERE beneficiaireID=\''.$_POST['club'].'\';');
	while ($address=$result->fetch()) {
		$beneficiaire->ajoutadresse(new Adresse($address['numero'],$address['voie'],$address['suite'],$address['codePostal'],$address['commune']),$address['surnom']);
	}


	if(count($beneficiaire->comptes)){
		$pageTitle='Profil de '.$_POST['club'].'-'.$beneficiaire->Commune;
		$ok=true;
	}
	else{
		$ok=false;
		$open=' open';
		$pageTitle='Profil de beneficiaire';
	}
}
?>



<!DOCTYPE HTML>
<html>
<?php require_once "../beneficiaire/head.php";?>
<body>

<!--------------------------header------------------------------------------------->
<?php 
	require_once "header.php";
if($ok){
?>


<section class="divers">
	<h3> Informations générales </h3>
<pre>
nom du club : <?= $beneficiaire->nom ?> 
identifiant : <?= $beneficiaire->getID() ?> 
Commune     : <?= $beneficiaire->Commune ?> 
mandataire  : <?= $beneficiaire->mandataire ?> 
email       : <?= $beneficiaire->email ?>
</pre>
</section>

<!--br><br-->
<section class="divers">
	<h3> Comptes </h3>
	<?php 
	foreach ($beneficiaire->comptes as $compte) {
		echo "<div>Compte : ".$compte->type ."<br>solde : ". $compte->solde().' €'. $compte->printEC()."</div><br>";
	}
	?>
</section>

<section class="divers">
	<h3> Adresses </h3>
<?php
	$vide=true;
	foreach ($beneficiaire->adresses as $key => $adresse) {
		$vide=false;?>
		<div class="adresse">
			<h3> <?= $key ?> </h3>
			<p><?= $adresse ?></p>
		</div>
	<?php
	}
	if($vide){
		echo "Aucune adresse enregistrée";
	}
	?><br><br>
</section>
<br><br><br>
<?php  } ?>
<br>
<details class="ajout" <?=$open?>>
	<summary>chercher un club</summary>
	<form method="POST" action="profilBeneficiaire.php">
		<input type="text" name="club" required>
		<input type="submit" name="submit" value="valider">
	</form>
</details>

</body>
</html>