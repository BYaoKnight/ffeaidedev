<?php
require_once "../backG/CheckConnection.php";
require_once "../back/connexionBD.php";
require_once "../back/classes1.php";
$pageTitle='Statistiques';
?>




<!DOCTYPE HTML>
<html>
<?php require_once "../beneficiaire/head.php";?>
<body>
<?php require_once "header.php"; ?>

<section class="mvtAtt">
	<h3>Activité des clubs</h3>
	<table>
		<tr>
			<th>Clubs</th>
			<th>nombre</th>
		</tr>
		<tr>
			<th>ayant commandé</th>
			<td>
				<?php
				$sql="select count(*) as nb FROM Beneficiaire WHERE ID NOT LIKE 'CJE%' AND ID IN (SELECT beneficiaireID FROM Mouvement WHERE ID>0) ;";
				$result=$bdd->query($sql);
				echo $result->fetch()['nb'];
				?>
			</td>
		</tr>
		<tr>
			<th>actifs</th>
			<td>
				<?php
				$sql="select count(*) as nb FROM clubs_actifs WHERE ID NOT LIKE 'CJE%' ;";
				$result=$bdd->query($sql);
				echo $result->fetch()['nb'];
				?>
			</td>
		</tr>
		<tr>
			<th>inactifs</th>
			<td>
				<?php
				$sql="select count(*) as nb FROM clubs_inactifs WHERE ID NOT LIKE 'CJE%' ;";
				$result=$bdd->query($sql);
				echo $result->fetch()['nb'];
				?>
			</td>
		</tr>
		<tr>
			<th>total</th>
			<td>
				<?php
				$sql="select count(*) as nb FROM Beneficiaire WHERE ID NOT LIKE 'CJE%' ;";
				$result=$bdd->query($sql);
				echo $result->fetch()['nb'];
				?>
			</td>
		</tr>
	</table>	
</section>
<section class="mvtAtt">
	<h3>Packs</h3>
	<table>
		<tr>
			<th>Packs</th>
			<th>Quantité commandée</th>
		</tr>
		<?php
		$sql="SELECT nom,count(*) as nb FROM Contenu JOIN Produit ON produit=ID WHERE beneficiaireID NOT LIKE 'CJE%'  AND mouvementID>0 GROUP BY nom ;";
		$result=$bdd->query($sql);
		while($rslt=$result->fetch()){
			echo "<tr><th>".$rslt['nom']."</th><td>".$rslt['nb']."</td></tr>";
		}
		?>
	</table>
</section>
</body>
</html>
