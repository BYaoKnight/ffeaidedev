<?php
require_once "../back/connexionBD.php";

//------------------------------new beneficiaire-----------------------------------------
$result=$bdd->query('SELECT * from Beneficiaire WHERE ID=\''.$_SESSION['beneficiaireID'].'\';');
$beneficiaire=$result->fetch();
$_SESSION['beneficiaire']=new Club($_SESSION['beneficiaireID'],$beneficiaire['Commune'],$beneficiaire['nom'],$beneficiaire['mandataire'],$beneficiaire['email']);


//--------------------------------new Comptes--------------------------------------------
$sql='SELECT DISTINCT nom, soldeInitial FROM Compte WHERE proprietaireID=\''.$_SESSION['beneficiaireID'].'\' ;';
$result=$bdd->query($sql);
//var_dump($sql);
while($comptes=$result->fetch()){
	$_SESSION['beneficiaire']->ajoutCompte(new Compte($comptes['nom'],$comptes['soldeInitial']));
}
foreach ($_SESSION['beneficiaire']->comptes as $key => &$compte) {
	$sql0="SELECT SUM(prix*quantite) as depense FROM Contenu 
	JOIN Produit ON produit=Produit.ID
	JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
	WHERE etat='valide' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$_SESSION['beneficiaireID']."';";

	$sql1="SELECT SUM(prix*quantite) as encours FROM Contenu 
	JOIN Produit ON produit=Produit.ID
	JOIN Avancement ON Avancement.mouvementID=Contenu.mouvementID AND Avancement.beneficiaireID=Contenu.beneficiaireID AND Avancement.compteID=Contenu.compteID
	WHERE etat='cree' AND Contenu.compteID='".$compte->type."' AND Contenu.beneficiaireID='".$_SESSION['beneficiaireID']."' AND Contenu.mouvementID NOT IN (SELECT mouvementID FROM Avancement as A1 WHERE A1.beneficiaireID='".$_SESSION['beneficiaireID']."' AND A1.compteID='".$compte->type."' AND etat<>'cree');";
	//echo "<pre>";
	$result=$bdd->query($sql0);
	$fetch=$result->fetch();
	$compte->solde=$compte->soldeInitial-(empty($fetch['depense'])?0:$fetch['depense']);

	$result=$bdd->query($sql1);
	$fetch=$result->fetch();
	$compte->encours=(empty($fetch['encours'])?0:$fetch['encours']);
	//var_dump($compte);
	//echo"</pre>";
}





//=====================================  new adresse  ====================================
$result=$bdd->query('SELECT `numero`,`voie`,`suite`,`codePostal`,`commune`,`surnom` FROM Adresse  WHERE beneficiaireID=\''.$_SESSION['beneficiaireID'].'\';');
while ($address=$result->fetch()) {
	$_SESSION['beneficiaire']->ajoutadresse(new Adresse($address['numero'],$address['voie'],$address['suite'],$address['codePostal'],$address['commune']),$address['surnom']);
}

?>