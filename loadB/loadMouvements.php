<?php
require_once "../back/connexionBD.php";

$_SESSION['catalogues']=array();

foreach ($_SESSION['beneficiaire']->comptes as $compte) {
	$result=$bdd->query('SELECT * FROM catalogue'.$_SESSION['beneficiaire']->type().$compte->type.';');
	while($produits=$result->fetch()){
		new Produit($produits['ID'],$produits['nom'],$produits['contenu'],$produits['prix'],$compte->type,$_SESSION['beneficiaire']->type());
	}

	//--------------------------------new mouvement------------------------------------------
	/*$sql='SELECT distinct moveID, etat,dateDeValeur  
		FROM FullDataFor'.$_SESSION['beneficiaireID'].' fD  
		WHERE compteID=\''.$compte->type.'\'  
			AND etat IN ( 
				SELECT etat FROM Avancement  
				WHERE Avancement.mouvementID=fD.moveID  
					AND Avancement.compteID=\''.$compte->type.'\'  
					AND Avancement.beneficiaireID=\''.$_SESSION['beneficiaireID'].'\'  
				ORDER BY etat DESC LIMIT 1 )  
		ORDER BY dateDeValeur DESC;';*/
	$sql='SELECT distinct Mouvement.ID as moveID, etat,dateDeValeur, commentaire  
		FROM Mouvement 
		JOIN Avancement ON Avancement.mouvementID=Mouvement.ID 
			AND Avancement.compteID=Mouvement.compteID
			AND Avancement.beneficiaireID=Mouvement.beneficiaireID
		WHERE Mouvement.compteID=\''.$compte->type.'\'  
			AND Mouvement.beneficiaireID=\''.$_SESSION['beneficiaireID'].'\' 
			AND etat=IFNULL( 
					(SELECT etat FROM Avancement as A1 
					WHERE A1.mouvementID=Mouvement.ID  
						AND A1.compteID=\''.$compte->type.'\'  
						AND A1.beneficiaireID=\''.$_SESSION['beneficiaireID'].'\' 
						AND A1.etat<>\'cree\'),
					\'cree\'
					)  
		ORDER BY dateDeValeur DESC;';
	//echo '<pre>'.$sql.'</pre>';
	$result=$bdd->query($sql);
	while($mouvement=$result->fetch()){
		$move=new Mouvement($mouvement['moveID'],$compte->type,$_SESSION['beneficiaireID'],$mouvement['commentaire']);
		$move->avancer(new avancement($mouvement['dateDeValeur'],$mouvement['etat']));
		$compte->ajoutMouvement($move);

	}

	foreach ($compte->mouvements() as &$move) {
		//-------------------------add produits to move----------------------------------------
		$sql='SELECT * FROM Contenu 
			JOIN Produit ON produit=ID 
			WHERE compteID=\''.$compte->type.'\' AND beneficiaireID=\''.$_SESSION['beneficiaireID'].'\' AND mouvementID=\''.$move->ID().'\';';
		//echo "<pre>$sql</pre>";
		$result=$bdd->query($sql);
		while($prod=$result->fetch()){
			$move->ajouterProduit(new Produit($prod['ID'],$prod['nom'],$prod['contenu'],$prod['prix']),$prod['quantite']);
		}
		//var_dump($move->produitsQtt());
	}

	

	$_SESSION['catalogues']=Produit::$catalogues;
}


?>