<?php
require_once "../back/connexionBD.php";

foreach ($_SESSION['beneficiaire']->comptes as &$compte) {
	$result=$bdd->query('SELECT * FROM catalogue'.$_SESSION['beneficiaire']->type().$compte->type.';');
	while($produits=$result->fetch()){
		new Produit($produits['ID'],$produits['nom'],$produits['contenu'],$produits['prix'],$compte->type,$_SESSION['beneficiaire']->type());
	}

	//-------------------------------- new mouvement ------------------------------------------
	$sql="SELECT * FROM WaitingMoves 
		WHERE compte='".$compte->type."' AND beneficiaire='".$_SESSION['beneficiaireID']."' 
		ORDER BY dateDeValeur DESC;";
	//echo '<pre>'.$sql.'</pre>';
	$result=$bdd->query($sql);
	while($mouvement=$result->fetch()){
		$move=new Mouvement($mouvement['mvtID'],$compte->type,$_SESSION['beneficiaireID'],$mouvement['commentaire']);
		$move->avancer(new avancement($mouvement['dateDeValeur'],'en attente'));
		$compte->ajoutMouvement($move);
	}

	foreach ($compte->mouvements() as &$move) {
		//------------------------- add produits to move ----------------------------------------
		$sql='SELECT * FROM Contenu 
			JOIN Produit ON produit=ID 
			WHERE compteID=\''.$compte->type.'\' AND beneficiaireID=\''.$_SESSION['beneficiaireID'].'\' AND mouvementID=\''.$move->ID().'\';';
		//echo "<pre>$sql</pre>";
		$result=$bdd->query($sql);
		while($prod=$result->fetch()){
			$move->ajouterProduit(new Produit($prod['ID'],$prod['nom'],$prod['contenu'],$prod['prix']),$prod['quantite']);
		}
		//var_dump($move->produitsQtt());
	}
}
$_SESSION['catalogues']=Produit::$catalogues;

?>